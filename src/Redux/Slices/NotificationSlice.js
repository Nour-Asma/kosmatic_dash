import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  open: false,
  message: "",
  status: "",
};

const NotificationSlice = createSlice({
  name: "sidebar",
  initialState,
  reducers: {
    openNotification: (state, { payload }) => {
      state.open = true;
      state.message = payload.message;
      state.status = payload.status;
    },
    closeNotification: (state, { payload }) => {
      state.open = false;
      state.message = payload;
    },
  },
});

export default NotificationSlice;
export const { closeNotification, openNotification } =
  NotificationSlice.actions;
