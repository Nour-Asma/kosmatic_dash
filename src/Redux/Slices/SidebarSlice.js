import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  open: true,
};

const SidebarSlice = createSlice({
  name: "sidebar",
  initialState,
  reducers: {
    toggleSidebar: (state) => {
      state.open = true;
    },
    closeSideBar: (state) => {
      state.open = false;
    },
  },
});

export const { toggleSidebar, closeSideBar } = SidebarSlice.actions;

export default SidebarSlice;
