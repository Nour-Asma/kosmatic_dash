import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_URL } from "../../data/BaseURL";

export const CategoryAPI = createApi({
  reducerPath: "category",
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      headers.append("Accept", "application/json");
      headers.set("ngrok-skip-browser-warning", true);
      if (localStorage.getItem("token")) {
        headers.set("Authorization", `Bearer ${localStorage.getItem("token")}`);
      }
    },
  }),
  endpoints: (builder) => ({
    getCategory: builder.query({
      query: () => ({
        url: "admin-category",
      }),
    }),
    createCategory: builder.mutation({
      query: (categoryData) => ({
        url: "category/store",
        method: "POST",
        body: categoryData,
      }),
    }),
    editCategory: builder.mutation({
      query: (categoryData) => ({
        url: `category/update/${categoryData.id}`,
        method: "PUT",
        body: { name: categoryData.name },
      }),
    }),
    getCategoryDetails: builder.query({
      query: (categoryId) => ({
        url: `admin-category/${categoryId}`,
      }),
    }),
    deleteCategory: builder.mutation({
      query: (categoryId) => ({
        url: `category/delete/${categoryId}`,
        method: "DELETE",
        body: {},
      }),
    }),
  }),
});

export const {
  useGetCategoryQuery,
  useCreateCategoryMutation,
  useEditCategoryMutation,
  useGetCategoryDetailsQuery,
  useDeleteCategoryMutation,
} = CategoryAPI;
