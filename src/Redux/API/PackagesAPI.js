import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_URL } from "../../data/BaseURL";
import { getFromattedDate } from "../../UI/DateUI";

export const PackagesAPI = createApi({
  reducerPath: "packages",
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      headers.append("Accept", "application/json");
      headers.set("ngrok-skip-browser-warning", true);

      if (localStorage.getItem("token")) {
        headers.set("Authorization", `Bearer ${localStorage.getItem("token")}`);
      }
    },
  }),
  endpoints: (builder) => ({
    getPackages: builder.query({
      query: () => ({
        url: "package-admin",
      }),
    }),
    createPackage: builder.mutation({
      query: (packageData) => {
        const formData = new FormData();
        formData.append("title", packageData.title);
        formData.append("discount", packageData.discount);
        formData.append("price", packageData.price);
        formData.append("address", packageData.address);
        formData.append("description", packageData.description);
        for (let i = 0; i < packageData.categories.length; i++) {
          formData.append("categories[]", packageData.categories[i]);
        }
        for (let i = 0; i < packageData.courses.length; i++) {
          formData.append("courses[]", packageData.courses[i]);
        }

        formData.append("src", packageData.src);

        return {
          url: "package/store",
          method: "POST",
          body: formData,
        };
      },
    }),
    editpackage: builder.mutation({
      query: (packageData) => {
        return {
          url: `package/update/${packageData.id}`,
          method: "PUT",
          body: packageData,
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: "packageDetails", id: arg.id },
      ],
    }),
    editPackagePic: builder.mutation({
      query: (packageData) => {
        const formData = new FormData();
        formData.append("src", packageData.src);
        return {
          url: `package-update-image/${packageData.id}`,
          method: "POST",
          body: formData,
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: "packageDetails", id: arg.id },
      ],
    }),
    getPackageDetails: builder.query({
      query: (packageId) => ({
        url: `package-admin/${packageId}`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [
        { id: arg, type: "packageDetails" },
      ],
    }),
    switchActivity: builder.mutation({
      query: (packageId) => ({
        url: `package/switch/${packageId}`,
        method: "PATCH",
        body: {},
      }),
    }),
    deletePackageDate: builder.mutation({
      query: (packageDate) => ({
        url: `package-date/delete/${packageDate}`,
        method: "DELETE",
        body: {},
      }),
    }),

    createPackageDate: builder.mutation({
      query: (dateData) => {
        const data = {
          from_date: getFromattedDate(dateData.from_date),
          to_date: getFromattedDate(dateData.to_date),
          package_id: dateData.package_id,
          location: dateData.location,
        };

        return {
          url: "package-date/store",
          method: "POST",
          body: data,
        };
      },
    }),

    deletePackageCategory: builder.mutation({
      query: (packageCategory) => ({
        url: `package-category/delete`,
        method: "DELETE",
        body: packageCategory,
      }),
    }),

    addPackageCategory: builder.mutation({
      query: (packageCategory) => ({
        url: `package-category/store`,
        method: "POST",
        body: packageCategory,
      }),
    }),

    addCoursePackage: builder.mutation({
      query: (corsePackage) => ({
        url: `package-course/store`,
        method: "POST",
        body: corsePackage,
      }),
    }),
    deleteCoursePackage: builder.mutation({
      query: (corsePackage) => ({
        url: `package-course/delete`,
        method: "DELETE",
        body: corsePackage,
      }),
    }),

    updatePackageDate: builder.mutation({
      query: (dateData) => {
        const data = {
          from_date: getFromattedDate(dateData.from_date),
          to_date: getFromattedDate(dateData.to_date),
          location: dateData.location,
        };
        return {
          url: "package-date/update/" + dateData.id,
          method: "PATCH",
          body: data,
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: "packageDetails", id: arg.package_id },
      ],
    }),
  }),
});

export const {
  useGetPackagesQuery,
  useCreatePackageMutation,
  useGetPackageDetailsQuery,
  useSwitchActivityMutation,
  useDeletePackageDateMutation,
  useCreatePackageDateMutation,
  useEditPackagePicMutation,
  useEditpackageMutation,
  useDeletePackageCategoryMutation,
  useAddPackageCategoryMutation,
  useDeleteCoursePackageMutation,
  useAddCoursePackageMutation,
  useUpdatePackageDateMutation,
} = PackagesAPI;
