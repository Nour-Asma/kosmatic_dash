import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { BASE_URL } from "../../data/BaseURL";

export const RegistrationAPI = createApi({
  reducerPath: "registrations",
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      headers.append("Accept", "application/json");
      headers.set("ngrok-skip-browser-warning", true);

      if (localStorage.getItem("token")) {
        headers.set("Authorization", `Bearer ${localStorage.getItem("token")}`);
      }
    },
  }),
  endpoints: (builder) => ({
    getRegistration: builder.query({
      query: () => ({
        url: "registration",
      }),
    }),
    downloadContract: builder.mutation({
      query: (id) => ({
        url: `contract/${id}`,
      }),
    }),
    confirmRegistration: builder.mutation({
      query: (registrationId) => ({
        url: `confirm-registration/${registrationId}`,
        method: "PUT",
      }),
    }),
    rejectRegistration: builder.mutation({
      query: (registrationId) => ({
        url: `reject-registration/${registrationId}`,
        method: "PUT",
      }),
    }),
  }),
});
export const {
  useGetRegistrationQuery,
  useDownloadContractMutation,
  useConfirmRegistrationMutation,
  useRejectRegistrationMutation,
} = RegistrationAPI;
