import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_URL } from "../../data/BaseURL";

export const CourseAPI = createApi({
  reducerPath: "course",
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      headers.append("Accept", "application/json");
      headers.set("ngrok-skip-browser-warning", true);

      if (localStorage.getItem("token")) {
        headers.set("Authorization", `Bearer ${localStorage.getItem("token")}`);
      }
    },
  }),
  endpoints: (builder) => ({
    getCourse: builder.query({
      query: () => ({
        url: "admin-course",
      }),
    }),
    createCourse: builder.mutation({
      query: (courseData) => {
        const formData = new FormData();
        formData.append("title", courseData.title);
        formData.append("address", courseData.address);
        formData.append("discount", courseData.discount);
        formData.append("price", courseData.price);

        for (let i = 0; i < courseData.categories.length; i++) {
          formData.append("categories[]", courseData.categories[i]);
        }

        formData.append("src", courseData.src);

        return {
          url: "course/store",
          method: "POST",
          body: formData,
        };
      },
    }),

    editCourse: builder.mutation({
      query: (courseData) => {
        return {
          url: `course/update/${courseData.id}`,
          method: "PUT",
          body: courseData,
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: "courseDetails", id: arg.id },
      ],
    }),

    editCoursePic: builder.mutation({
      query: (courseData) => {
        const formData = new FormData();
        formData.append("src", courseData.src);
        return {
          url: `course-update-image/${courseData.id}`,
          method: "POST",
          body: formData,
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: "courseDetails", id: arg.id },
      ],
    }),

    createCourseSection: builder.mutation({
      query: (sectionData) => {
        const formData = new FormData();
        formData.append("title", sectionData.title);
        formData.append("information", sectionData.information);
        if (sectionData.src) {
          formData.append("src", sectionData.src);
        }
        formData.append("course_id", sectionData.course_id);

        return {
          url: "course-sections/store",
          method: "POST",
          body: formData,
        };
      },
    }),
    updateCourseSection: builder.mutation({
      query: (sectionData) => {
        const formData = new FormData();
        if (sectionData.title) formData.append("title", sectionData.title);
        if (sectionData.information)
          formData.append("information", sectionData.information);
        if (sectionData.src) {
          formData.append("src", sectionData.src);
        }

        return {
          url: `course-sections/update/${sectionData.course_id}`,
          method: "POST",
          body: formData,
        };
      },
    }),
    deleteCourseSection: builder.mutation({
      query: (courseId) => ({
        url: `course-sections/delete/${courseId}`,
        method: "DELETE",
        body: {},
      }),
    }),
    switchActivity: builder.mutation({
      query: (courseId) => ({
        url: `course-active/${courseId}`,
        method: "PUT",
        body: {},
      }),
    }),
    getCourseDetails: builder.query({
      query: (courseId) => ({
        url: `course-with-sections-admin/${courseId}`,
        method: "GET",
      }),
      providesTags: (result, error, arg) => [
        { id: arg, type: "courseDetails" },
      ],
    }),
    deleteCourseDate: builder.mutation({
      query: (courseDateId) => ({
        url: `course-date/delete/${courseDateId}`,
        method: "DELETE",
        body: {},
      }),
    }),
    createCourseDate: builder.mutation({
      query: (dateData) => {
        const data = {
          from_date: getFromattedDate(dateData.from_date),
          to_date: getFromattedDate(dateData.to_date),
          course_id: dateData.course_id,
          location: dateData.location,
        };
        return {
          url: "course-date/store",
          method: "POST",
          body: data,
        };
      },
    }),

    updateCourseDate: builder.mutation({
      query: (dateData) => {
        const data = {
          from_date: getFromattedDate(dateData.from_date),
          to_date: getFromattedDate(dateData.to_date),
          location: dateData.location,
        };
        return {
          url: "course-date/update/" + dateData.id,
          method: "PATCH",
          body: data,
        };
      },
      invalidatesTags: (result, error, arg) => [
        { type: "courseDetails", id: arg.course_id },
      ],
    }),

    addCourseCategory: builder.mutation({
      query: (courseCategory) => ({
        url: `course-category/store`,
        method: "POST",
        body: courseCategory,
      }),
    }),
    deleteCourseCategory: builder.mutation({
      query: (courseCategory) => ({
        url: `course-category/delete`,
        method: "DELETE",
        body: courseCategory,
      }),
    }),
  }),
});

export const {
  useGetCourseQuery,
  useCreateCourseMutation,
  useSwitchActivityMutation,
  useGetCourseDetailsQuery,
  useCreateCourseSectionMutation,
  useDeleteCourseSectionMutation,
  useUpdateCourseSectionMutation,
  useDeleteCourseDateMutation,
  useCreateCourseDateMutation,
  useEditCourseMutation,
  useEditCoursePicMutation,
  useDeleteCourseCategoryMutation,
  useAddCourseCategoryMutation,
  useUpdateCourseDateMutation,
} = CourseAPI;

const getFromattedDate = (date) => {
  const month = date.getUTCMonth() + 1; // Months are zero-indexed, so add 1
  const day = date.getUTCDate();
  const year = date.getUTCFullYear();

  const formattedDate = `${day}-${month}-${year}`;
  return formattedDate;
};
