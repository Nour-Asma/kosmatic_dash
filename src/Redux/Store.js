import { configureStore } from "@reduxjs/toolkit";

import SidebarSlice from "./Slices/SidebarSlice";
import AuthSlice from "./Slices/AuthSlice";
import { AuthAPI } from "./API/AuthAPI";
import { setupListeners } from "@reduxjs/toolkit/query";
import { CategoryAPI } from "./API/CategoryAPI";
import { CourseAPI } from "./API/CourseAPI";
import { PackagesAPI } from "./API/PackagesAPI";
import { RegistrationAPI } from "./API/RegistrationAPI";
import NotificationSlice from "./Slices/NotificationSlice";

const store = configureStore({
  reducer: {
    sidebar: SidebarSlice.reducer,
    auth: AuthSlice.reducer,
    notification: NotificationSlice.reducer,
    [AuthAPI.reducerPath]: AuthAPI.reducer,
    [CategoryAPI.reducerPath]: CategoryAPI.reducer,
    [CourseAPI.reducerPath]: CourseAPI.reducer,
    [PackagesAPI.reducerPath]: PackagesAPI.reducer,
    [RegistrationAPI.reducerPath]: RegistrationAPI.reducer,
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      AuthAPI.middleware,
      CategoryAPI.middleware,
      CourseAPI.middleware,
      PackagesAPI.middleware,
      RegistrationAPI.middleware
    ),
});

setupListeners(store.dispatch);

export default store;
