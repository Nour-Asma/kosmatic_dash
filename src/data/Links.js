import { MdOutlineLibraryBooks } from "react-icons/md";
import { BiSolidCategoryAlt } from "react-icons/bi";
import { IoBookSharp } from "react-icons/io5";
import { LuPackageOpen } from "react-icons/lu";
import { IoSettings } from "react-icons/io5";
export const links = [
  {
    name: "registration",
    icon: (
      <MdOutlineLibraryBooks
        style={{
          fontWeight: "bold",
          fontSize: "1.3rem",
          marginRight: "5px",
        }}
      />
    ),
  },
  {
    name: "categories",
    icon: (
      <BiSolidCategoryAlt
        style={{
          fontWeight: "bold",
          fontSize: "1.3rem",
          marginRight: "5px",
        }}
      />
    ),
  },
  {
    name: "courses",
    icon: (
      <IoBookSharp
        style={{
          fontWeight: "bold",
          fontSize: "1.3rem",
          marginRight: "5px",
        }}
      />
    ),
  },
  {
    name: "packages",
    icon: (
      <LuPackageOpen
        style={{
          fontWeight: "bold",
          fontSize: "1.3rem",
          marginRight: "5px",
        }}
      />
    ),
  },
  {
    name: "settings",
    icon: (
      <IoSettings
        style={{
          fontWeight: "bold",
          fontSize: "1.3rem",
          marginRight: "5px",
        }}
      />
    ),
  },
];
