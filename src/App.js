import "./App.css";
import ThemeRoutes from "./Routes/Routes";
import { ThemeProvider, createTheme } from "@mui/material";
import { ThemeColor } from "./Theme/Theme";
import Notification from "./UI/Notification";

function App() {
  const theme = createTheme({
    palette: {
      primary: {
        main: ThemeColor.secondary,
      },
    },
    typography: {
      title: {
        color: ThemeColor.main,
        fontSize: "0.9rem",
        display: "block",
      },

      linkSelected: {
        color: ThemeColor.secondary,
        backgroundColor: ThemeColor.bodyBackground,
        fontSize: "0.9rem",
        display: "block",
      },
    },
  });

  return (
    <div className="App" style={{ background: ThemeColor.bodyBackground }}>
      <ThemeProvider theme={theme}>
        <ThemeRoutes />
        <Notification />
      </ThemeProvider>
    </div>
  );
}

export default App;
