import { Box, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { ThemeColor } from "../Theme/Theme";

const PopupLayout = ({ children, closePopup, title }) => {
  const [width, setWidth] = useState(window.innerWidth);

  function handleResize() {
    setWidth(window.innerWidth);
  }

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <Box
      sx={{
        position: "fixed",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        width: "100%",
        height: "100vh",
        background: "#000000d6",
        zIndex: "1000",
        boxShadow: "inset 0px 0px 20px 9px #333",
      }}
      onClick={(e) => {
        if (e.target === e.currentTarget) {
          closePopup();
        }
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%,-50%)",
          background: ThemeColor.bodyBackground,
          padding: "1rem",
          width: width < 400 ? "90%" : "400px",
          opacity: "1",
          minHeight: "200px",
          maxHeight: "80%",
          overflowY: "auto",
          borderRadius: "5px",
          boxShadow: ThemeColor.boxShadow,
        }}
      >
        <Typography
          variant="title"
          sx={{
            textTransform: "capitalize",
            fontSize: "1rem",
            marginBottom: "1rem",
            color: ThemeColor.title,
            fontWeight: "bold",
          }}
        >
          {title}
        </Typography>
        {children}
      </Box>
    </Box>
  );
};

export default PopupLayout;
