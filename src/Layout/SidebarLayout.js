import { Grid } from "@mui/material";
import React from "react";
import SideBar from "../UI/SideBar";
import Header from "../UI/Header";
import { useSelector } from "react-redux";
import { Outlet } from "react-router-dom";

const SidebarLayout = () => {
  const { open } = useSelector((state) => state.sidebar);

  return (
    <Grid container>
      <Grid
        sx={{ transition: "0.5s" }}
        item
        lg={open ? 2 : 1}
        md={open ? 2.5 : 1}
        sm={open ? 3.8 : 1.5}
        xs={0}
      >
        <SideBar />
      </Grid>

      <Grid
        justifySelf={"center"}
        sx={{ transition: "0.5s", margin: "0 auto" }}
        item
        lg={open ? 10 : 11}
        md={open ? 9.5 : 11}
        sm={open ? 8.2 : 10.5}
        xs={12}
      >
        <Header />
        <Outlet />
      </Grid>
    </Grid>
  );
};

export default SidebarLayout;
