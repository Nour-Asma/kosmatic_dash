import SidebarLayout from "../Layout/SidebarLayout";
import PrivateRoutes from "./PrivateRoutes";
import Categories from "../Pages/Categories";
import Courses from "../Pages/Courses";
import CourseDetails from "../Pages/CourseDetails/CourseDetails";
import Packages from "../Pages/Packages/Packages";
import PackageDetails from "../Pages/PackageDetails/PackageDetails";
import CategoryDetails from "../Pages/CategoryDetails/CategoryDetails";
import Registration from "../Pages/Registration/Registration";
import Setting from "../Pages/Setting/Setting";

const AuthenticationRoutes = {
  element: <PrivateRoutes />,

  children: [
    {
      element: <SidebarLayout></SidebarLayout>,
      path: "dashboard",
      children: [
        {
          element: <Categories />,
          path: "categories",
        },
        {
          element: <CategoryDetails />,
          path: "category/:id",
        },
        {
          element: <Registration />,
          path: "registration",
        },
        {
          element: <Courses />,
          path: "courses",
        },
        { element: <CourseDetails />, path: "course/:id" },
        {
          element: <Packages />,
          path: "packages",
        },
        {
          element: <PackageDetails />,
          path: "package/:id",
        },
        {
          element: <Setting />,
          path: "settings",
        },
      ],
    },
  ],
};
export default AuthenticationRoutes;
