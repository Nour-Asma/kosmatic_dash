import React, { forwardRef, useState } from "react";
import PopupLayout from "../../Layout/PopupLayout";
import { Button, Stack, TextField, Typography, Box } from "@mui/material";
import DatePicker from "react-datepicker";
import { ThemeColor } from "../../Theme/Theme";
import NorthIcon from "@mui/icons-material/North";
import {
  useEditCourseMutation,
  useUpdateCourseDateMutation,
} from "../../Redux/API/CourseAPI";
import Loader from "../Loader";
import { useUpdatePackageDateMutation } from "../../Redux/API/PackagesAPI";

const EditDate = ({ closePopup, course_id, package_id, data }) => {
  const [dateInfo, setDateInfo] = useState({
    from_date: new Date(data.from_date),
    to_date: new Date(data.to_date),
    location: data.location,
    id: data.id,
    course_id,
    package_id,
  });

  const [updateCourseDate, { isLoading: updateCourseDateLoading }] =
    useUpdateCourseDateMutation();

  const [updatePackageDate, { isLoading: updatePackageDateLoading }] =
    useUpdatePackageDateMutation();

  const handleEditDate = (e) => {
    e.preventDefault();

    if (course_id) {
      updateCourseDate(dateInfo).then(() => {
        closePopup();
      });
    } else if (package_id) {
      updatePackageDate(dateInfo).then(() => {
        closePopup();
      });
    }
  };

  return (
    <PopupLayout title={"Add new Date"} closePopup={closePopup}>
      {updateCourseDateLoading || updatePackageDateLoading ? (
        <Loader />
      ) : (
        <form onSubmit={handleEditDate}>
          <Stack alignItems={"center"} gap={"1.4rem"} height={"400px"}>
            <Box pt={"0.5rem"}>
              <Typography sx={{ fontWeight: "bold", textAlign: "center" }}>
                Start At
              </Typography>
              <DatePicker
                selected={dateInfo.from_date}
                onChange={(date) => {
                  setDateInfo({
                    ...dateInfo,
                    from_date: date,
                  });
                }}
                customInput={<DateCustomeInput onClick={() => {}} />}
                dateFormat="dd/MM/yyyy"
              />
            </Box>
            <Box>
              <NorthIcon sx={{ transform: "rotate(180deg)" }} />
            </Box>
            <Box>
              <Typography sx={{ fontWeight: "bold", textAlign: "center" }}>
                End At
              </Typography>
              <DatePicker
                selected={dateInfo.to_date}
                onChange={(date) => {
                  setDateInfo({
                    ...dateInfo,
                    to_date: date,
                  });
                }}
                dateFormat="dd/MM/yyyy"
                customInput={<DateCustomeInput onClick={() => {}} />}
              />
            </Box>
            <Box>
              <TextField
                name="location"
                required
                fullWidth
                size="small"
                label="location"
                value={dateInfo.location}
                onChange={(e) => {
                  setDateInfo({ ...dateInfo, location: e.target.value });
                }}
              />
            </Box>
            <div style={{ margin: "1rem 0" }}>
              <Button variant={"contained"} size={"small"} type="submit">
                Edit
              </Button>
            </div>
          </Stack>
        </form>
      )}
    </PopupLayout>
  );
};

const DateCustomeInput = forwardRef(({ value, onClick }, ref) => (
  <Box
    sx={{
      background: ThemeColor.secondary,
      color: "white",
      width: "fit-content",
      padding: "0.3rem 0.6rem",
      cursor: "pointer",
      borderRadius: "5px",
    }}
    onClick={onClick}
    ref={ref}
  >
    {value}
  </Box>
));

export default EditDate;
