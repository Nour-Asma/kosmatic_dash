import React, { useRef, useState, useEffect } from "react";
import { Box, Stack, Typography } from "@mui/material";
import { Link, useLocation, useNavigate } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { ThemeColor } from "../Theme/Theme";
import { links } from "../data/Links";
import LogoutIcon from "@mui/icons-material/Logout";
import { useLogoutMutation } from "../Redux/API/AuthAPI";
import { closeSideBar, toggleSidebar } from "../Redux/Slices/SidebarSlice";

const SideBar = () => {
  const location = useLocation();
  const path = location.pathname.split("/").pop();
  const { open } = useSelector((state) => state.sidebar);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const ref = useRef();

  useEffect(() => {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        // Perform your action here
        if (event.target.id != "sideBarOpen") {
          dispatch(closeSideBar());
        }
      }
    }

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const [logout] = useLogoutMutation();
  const handleLogout = () => {
    logout().then((res) => {
      localStorage.clear();
      navigate("/");
    });
  };
  return (
    <Stack
      ref={ref}
      sx={{
        background: `${ThemeColor.layoutBackground}`,
        minHeight: "94vh",
        width: open ? "190px" : "62px",
        overflow: "hidden",
        transition: "0.3s",
        borderRadius: "5px",
        position: "fixed",
        top: "50%",
        transform: "translateY(-50%)",
        boxShadow: {
          sm: "-14px 8px 10px 0 #262323",
          xs: "14px 2px 20px 1px #262323",
        },
        zIndex: "100",
        transform: {
          sm: "translate(0 , -50%)",
          xs: open ? "translate(0 , -50%)" : "translate(-130% , -50%)",
        },
      }}
      p={"1rem"}
      pr={"0"}
      gap={"2rem"}
    >
      {links.map((item, index) => {
        return (
          <Typography
            variant={path === item.name ? "linkSelected" : "title"}
            key={index}
            sx={{
              textTransform: "capitalize",
              cursor: "pointer",
              padding: "0.4rem",
              borderRadius: "10px 0 0 10px ",
              width: "fit-content",
              fontWeight: "600",
              paddingRight: "0",
              width: "100%",
            }}
          >
            <Link
              to={`${item.name}`}
              style={{ width: "100%", position: "relative" }}
            >
              <Stack
                direction={"row"}
                gap={"1rem"}
                sx={{ position: "relative" }}
                alignItems={"center"}
              >
                {path === item.name && (
                  <Box
                    sx={{
                      position: "absolute",
                      width: "100%",
                      height: "20px",
                      cursor: "default",
                      top: "-25px",
                      background: ThemeColor.bodyBackground,
                      "&:after": {
                        content: `" "`,
                        width: "100%",
                        height: "100%",
                        background: ThemeColor.layoutBackground,
                        position: "absolute",
                        borderBottomRightRadius: {
                          sm: "20px",
                          xs: "0",
                        },
                        top: "-1px",
                        left: "0",
                        zIndex: "1",
                        // display: "none",
                      },
                    }}
                  ></Box>
                )}

                <Box>{item.icon}</Box>
                <Box>{item.name}</Box>
                {path === item.name && (
                  <Box
                    sx={{
                      position: "absolute",
                      width: "100%",
                      height: "20px",
                      cursor: "default",
                      bottom: "-25px",
                      background: ThemeColor.bodyBackground,
                      "&:after": {
                        content: `" "`,
                        width: "100%",
                        height: "100%",
                        background: ThemeColor.layoutBackground,
                        position: "absolute",
                        borderTopRightRadius: {
                          sm: "20px",
                          xs: "0",
                        },
                        bottom: "-1px",
                        left: "0",
                        zIndex: "1",
                        // display: "none",
                      },
                    }}
                  ></Box>
                )}
              </Stack>
            </Link>
          </Typography>
        );
      })}
      <Box
        sx={{
          cursor: "pointer",
          transition: "0.4s",
          padding: "0.5rem 0 ",
          "&:hover": {
            background: ThemeColor.bodyBackground,
          },
        }}
        onClick={handleLogout}
      >
        <Stack
          direction={"row"}
          gap={"1rem"}
          alignItems={"center"}
          sx={{ position: "relative" }}
        >
          <LogoutIcon sx={{ transform: "rotateY(180deg) scale(0.8)" }} />
          <Box
            sx={{
              fontWeight: "600",
              fontSize: "0.8rem",
              marginLeft: "0.4rem",
            }}
          >
            Logout
          </Box>
        </Stack>
      </Box>
    </Stack>
  );
};

export default SideBar;
