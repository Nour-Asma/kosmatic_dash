import { Box, Stack } from "@mui/material";
import React from "react";
import { useTheme } from "@emotion/react";
import ListIcon from "@mui/icons-material/List";
import { useDispatch, useSelector } from "react-redux";
import { closeSideBar, toggleSidebar } from "../Redux/Slices/SidebarSlice";
import { ThemeColor } from "../Theme/Theme";
import Logo from "../images/Logo.png";

const Header = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { open } = useSelector((state) => state.sidebar);

  return (
    <Stack
      direction={"row"}
      justifyContent={"space-between"}
      alignItems={"center"}
      sx={{
        width: "100%",
        padding: "1rem",

        top: "0",
        background: ThemeColor.bodyBackground,
      }}
    >
      <Stack
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        sx={{
          border: `2px solid ${theme.palette.primary.main}`,
          padding: "1.5rem",
          borderRadius: "10px",
          width: "100%",
        }}
      >
        <Box>
          <img style={{ maxWidth: "7%" }} src={Logo} />
        </Box>
        <Box>
          <ListIcon
            sx={{ color: `${theme.palette.primary.main}`, cursor: "pointer" }}
            id={"sideBarOpen"}
            onClick={() => {
              if (open) {
                dispatch(closeSideBar());
              } else dispatch(toggleSidebar());
            }}
          />
        </Box>
      </Stack>
    </Stack>
  );
};

export default Header;
