import React from "react";
import { ThemeColor } from "../Theme/Theme";

const Button = (props) => {
  return (
    <button
      style={{
        background: props.disabled ? "rgb(163 163 163)" : ThemeColor.secondary,
        fontSize: "1rem",
        padding: "0.5rem 1rem",
        color: props.disabled ? "" : "white",
        outline: "none",
        borderRadius: "5px",
        border: "none",
        cursor: "pointer",
      }}
      {...props}
    >
      {props.children}
    </button>
  );
};

export default Button;
