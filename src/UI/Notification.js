import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeNotification } from "../Redux/Slices/NotificationSlice";
import { Alert, Snackbar } from "@mui/material";

const Notification = () => {
  const { open, message, status } = useSelector((state) => state.notification);
  const dispatch = useDispatch();
  const closeSnackbar = () => {
    dispatch(closeNotification());
  };
  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={closeSnackbar}>
      <Alert
        onClose={closeSnackbar}
        severity={status}
        variant="filled"
        sx={{ width: "100%" }}
      >
        {message}
      </Alert>
    </Snackbar>
  );
};

export default Notification;
