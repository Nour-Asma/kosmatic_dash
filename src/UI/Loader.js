import React from "react";
import { Box, CircularProgress } from "@mui/material";

const Loader = () => {
  return (
    <Box sx={{ display: "flex", justifyContent: "center", margin: "2rem" }}>
      <CircularProgress sx={{ color: "#333", margin: "0 auto" }} />
    </Box>
  );
};

export default Loader;
