import React, { forwardRef, useState } from "react";
import {
  Box,
  IconButton,
  Stack,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import DatePicker from "react-datepicker";
import NorthIcon from "@mui/icons-material/North";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  useCreateCourseDateMutation,
  useDeleteCourseDateMutation,
} from "../Redux/API/CourseAPI";
import { ThemeColor } from "../Theme/Theme";
import Loader from "./Loader";
import PopupLayout from "../Layout/PopupLayout";
import Button from "./Button";
import "react-datepicker/dist/react-datepicker.css";
import {
  useCreatePackageDateMutation,
  useDeletePackageDateMutation,
} from "../Redux/API/PackagesAPI";
import SmallButton from "./SmallButton";
import EditIcon from "@mui/icons-material/Edit";
import InfoPair from "./InfoPair";
import EditDate from "./Popups/EditDate";

const DateUI = ({ refetch, datesArray, course_id, package_id }) => {
  const [deleteCourseDate, { isLoading: deleteCourseDateLoading }] =
    useDeleteCourseDateMutation();

  const [deletePackageDate, { isLoading: deletePackageDateLoading }] =
    useDeletePackageDateMutation();

  const [popupState, setPopupState] = useState(false);
  const closePopup = () => {
    setPopupState(false);
  };
  const openPopup = () => {
    setPopupState(true);
  };

  const [editCoursePopup, setEditCoursePopup] = useState({ open: false });
  const closeEditCoursePopup = () => {
    setEditCoursePopup({ open: false });
  };
  const openEditCoursePopup = (data) => {
    setEditCoursePopup({ open: true, data });
  };

  const [dateInfo, setDateInfo] = useState({
    from_date: new Date(),
    to_date: new Date(),
    course_id,
    package_id,
    location: "",
  });

  const [createCourseDate, { isLoading: createDateLoading }] =
    useCreateCourseDateMutation();

  const [createPackageDate, { isLoading: createPackageDateLoading }] =
    useCreatePackageDateMutation();

  const handleCreateDate = (e) => {
    e.preventDefault();

    if (course_id) {
      createCourseDate(dateInfo).then(() => {
        refetch();
        closePopup();
      });
    } else if (package_id) {
      createPackageDate(dateInfo).then(() => {
        refetch();
        closePopup();
      });
    }
  };

  const DateCustomeInput = forwardRef(({ value, onClick }, ref) => (
    <Box
      sx={{
        background: ThemeColor.secondary,
        color: "white",
        width: "fit-content",
        padding: "0.3rem 0.6rem",
        cursor: "pointer",
        borderRadius: "5px",
      }}
      onClick={onClick}
      ref={ref}
    >
      {value}
    </Box>
  ));

  return (
    <Box>
      {popupState && (
        <PopupLayout title={"Add new Date"} closePopup={closePopup}>
          {createDateLoading || createPackageDateLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleCreateDate}>
              <Stack alignItems={"center"} gap={"1.4rem"} height={"400px"}>
                <Box pt={"0.5rem"}>
                  <Typography sx={{ fontWeight: "bold", textAlign: "center" }}>
                    Start At
                  </Typography>
                  <DatePicker
                    selected={dateInfo.from_date}
                    onChange={(date) => {
                      setDateInfo({
                        ...dateInfo,
                        from_date: date,
                      });
                    }}
                    customInput={<DateCustomeInput onClick={() => {}} />}
                    dateFormat="dd/MM/yyyy"
                  />
                </Box>
                <Box>
                  <NorthIcon sx={{ transform: "rotate(180deg)" }} />
                </Box>
                <Box>
                  <Typography sx={{ fontWeight: "bold", textAlign: "center" }}>
                    End At
                  </Typography>
                  <DatePicker
                    selected={dateInfo.to_date}
                    onChange={(date) => {
                      setDateInfo({
                        ...dateInfo,
                        to_date: date,
                      });
                    }}
                    dateFormat="dd/MM/yyyy"
                    customInput={<DateCustomeInput onClick={() => {}} />}
                  />
                </Box>
                <Box>
                  <TextField
                    name="location"
                    required
                    fullWidth
                    size="small"
                    label="location"
                    onChange={(e) => {
                      setDateInfo({
                        ...dateInfo,
                        location: e.target.value,
                      });
                    }}
                  />
                </Box>
                <div style={{ margin: "1rem 0" }}>
                  <Button type="submit">Create</Button>
                </div>
              </Stack>
            </form>
          )}
        </PopupLayout>
      )}

      {editCoursePopup.open && (
        <EditDate
          closePopup={closeEditCoursePopup}
          course_id={course_id}
          package_id={package_id}
          data={editCoursePopup.data}
        />
      )}

      <Stack direction={"row"} gap={"1rem"} flexWrap={"wrap"} m={"0.4rem"}>
        {datesArray?.map((item) => (
          <Stack
            sx={{
              border: "1px solid ",
              borderColor: ThemeColor.main,
              padding: "1rem 1.5rem",
              borderRadius: "10px",
              width: {
                md: "fit-content",
                xs: "100%",
              },
            }}
            gap={"0.5rem"}
            key={item.id}
          >
            <InfoPair title={"from"} info={<span>{item.from_date}</span>} />
            <InfoPair title={"to"} info={<span>{item.to_date}</span>} />
            <InfoPair title={"location"} info={<span>{item.location}</span>} />
            <Box sx={{ textAlign: "right" }}>
              <Tooltip title="Delete">
                <IconButton
                  onClick={() => {
                    if (course_id) {
                      deleteCourseDate(item.id).then(() => {
                        refetch();
                        closePopup();
                      });
                    } else if (package_id) {
                      deletePackageDate(item.id).then(() => {
                        refetch();
                        closePopup();
                      });
                    }
                  }}
                  disabled={deleteCourseDateLoading || deletePackageDateLoading}
                  aria-label="Delete"
                >
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="edit">
                <IconButton
                  disabled={deleteCourseDateLoading || deletePackageDateLoading}
                  aria-label="Edit"
                  onClick={() => {
                    openEditCoursePopup(item);
                  }}
                >
                  <EditIcon />
                </IconButton>
              </Tooltip>
            </Box>
          </Stack>
        ))}
      </Stack>
      <Box sx={{ margin: "1rem 0.4rem" }}>
        <SmallButton onClick={openPopup}>Add new date</SmallButton>
      </Box>
    </Box>
  );
};

export const getFromattedDate = (date) => {
  const month = date.getUTCMonth() + 1; // Months are zero-indexed, so add 1
  const day = date.getUTCDate();
  const year = date.getUTCFullYear();

  const formattedDate = `${day}-${month}-${year}`;
  return formattedDate;
};

export default DateUI;
