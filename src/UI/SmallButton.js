import React from "react";
import { ThemeColor } from "../Theme/Theme";
import AddIcon from "@mui/icons-material/Add";
function SmallButton(props) {
  return (
    <button
      style={{
        background: ThemeColor.secondary,
        color: "#eee",
        padding: "0.2rem 0.3rem",
        outline: "none",
        border: "none",
        borderRadius: "0.1rem",
        cursor: "pointer",
        display: "block",
        margin: "0.4rem 0 ",
        boxShadow: "0 2px 6px 0px #413f3ff7",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
      {...props}
    >
      <span>{props.children}</span>

      <AddIcon
        sx={{
          transform: "scale(0.7)",
        }}
      />
    </button>
  );
}

export default SmallButton;
