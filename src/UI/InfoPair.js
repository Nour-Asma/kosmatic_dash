import { Box } from "@mui/material";
import React from "react";

function InfoPair({ title, info }) {
  return (
    <Box>
      <span style={{ fontWeight: "bold" }}>{title} : </span>
      <span>{info}</span>
    </Box>
  );
}

export default InfoPair;
