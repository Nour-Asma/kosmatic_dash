export const ThemeColor = {
  main: "#0d0023",
  secondary: "#660919",
  title: "#0d0023",
  layoutBackground: "#ddd",
  bodyBackground: "#eee",
};
