import {
  Box,
  Button,
  CircularProgress,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import loginImage from "../../images/HOME.jpg";
import Logo from "../../images/Logo.png";
import FormControl from "@mui/material/FormControl";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useLoginMutation } from "../../Redux/API/AuthAPI";
import { useNavigate } from "react-router-dom";
import { ThemeColor } from "../../Theme/Theme";

const Login = () => {
  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const [login, { isLoading, error, data, isSuccess, isError }] =
    useLoginMutation();
  const navigate = useNavigate();

  const onChangeHandler = (event) => {
    setCredentials({
      ...credentials,
      [event.target.name]: event.target.value,
    });
  };

  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    login(credentials);
  };

  if (isSuccess) {
    localStorage.setItem("token", data.token);
    navigate("/dashboard/registration");
  }
  return (
    <Stack
      minHeight={"100vh"}
      padding={{ md: "0  0rem 0 2rem ", xs: "0 2rem" }}
      alignItems={"center"}
      justifyContent={""}
      direction={{ md: "row", sm: "column-reverse", xs: "column-reverse" }}
    >
      <Box sx={{ flex: "0.9", padding: { md: "1rem", sm: "0" } }}>
        <Box mb={"2rem"}>
          <Box
            sx={{
              textAlign: "center",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              gap: "1rem",
            }}
          >
            <Typography
              variant="h4"
              sx={{
                display: "inline-block",
                marginRight: "0.5rem",
                fontWeight: "bold",
                textAlign: "center",
                margin: "0.5rem 0",
              }}
            >
              Kosmetik
            </Typography>
            <img width={"10%"} src={Logo} />
          </Box>
        </Box>
        <form onSubmit={handleSubmit}>
          <Stack padding={{ lg: " 0  3rem", md: "0 " }} gap={"1rem"}>
            <TextField
              type="email"
              required
              name="email"
              onChange={onChangeHandler}
              label={"Email"}
            />

            <FormControl sx={{ width: "full" }} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                name="password"
                onChange={onChangeHandler}
                type={showPassword ? "text" : "password"}
                required
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
              />
            </FormControl>

            <Button
              sx={{
                padding: "0.5rem",
              }}
              type="submit"
              fullWidth
              variant="contained"
              disabled={isLoading}
            >
              {isLoading ? (
                <Box
                  sx={{
                    transform: "scale(0.5)",
                    margin: "0",
                    padding: "0",
                  }}
                >
                  <CircularProgress sx={{ color: "#fff" }} />
                </Box>
              ) : (
                <Box>Login</Box>
              )}
            </Button>
            {error?.status === 422 ? (
              <Typography
                sx={{ fontWeight: "bold", fontSize: "0.8rem" }}
                color={"error"}
              >
                Wrong Credentials
              </Typography>
            ) : (
              <></>
            )}
          </Stack>
        </form>
      </Box>

      <Box
        sx={{
          textAlign: "center",
          height: "100vh",
          flex: "1.1",
          backgroundImage: 'url("../../HOME.jpg")',
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      >
        {/* <img width={"100%"} src={loginImage} /> */}
      </Box>
    </Stack>
  );
};

export default Login;
