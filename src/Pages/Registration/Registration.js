import { Box, IconButton, Stack, Tooltip, Typography } from "@mui/material";
import React from "react";
import { ThemeColor } from "../../Theme/Theme";
import {
  useConfirmRegistrationMutation,
  useDownloadContractMutation,
  useGetRegistrationQuery,
  useRejectRegistrationMutation,
} from "../../Redux/API/RegistrationAPI";
import { Link } from "react-router-dom";
import Loader from "../../UI/Loader";
import Table from "../../UI/Table";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import { BASE_URL } from "../../data/BaseURL";
import TrendingFlatIcon from "@mui/icons-material/TrendingFlat";

function downloadFile(url, name) {
  fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  })
    .then((response) => response.blob())
    .then((response) => {
      const url = window.URL.createObjectURL(new Blob([response]));

      // const link = document.createElement("a");
      // link.href = url;
      // link.setAttribute("download", `${name}.pdf`); // or any other extension
      // document.body.appendChild(link);
      // link.click();

      let blob = new Blob([response], { type: "application/pdf" });
      let blobURL = URL.createObjectURL(blob);
      window.open(blobURL);
    });
}

const Registration = () => {
  const { data, isLoading, refetch } = useGetRegistrationQuery();

  const [confirmRegistration, { isLoading: confirmIsLoading }] =
    useConfirmRegistrationMutation();

  const [rejectRegistration, { isLoading: rejectIsLoading }] =
    useRejectRegistrationMutation();

  const columns_array_package = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "first_name",
      header: "First Name",
      size: 150,
    },
    {
      accessorKey: "last_name",
      header: "Last Name",
      size: 150,
    },
    {
      accessorKey: "email",
      header: "Email",
      size: 150,
    },
    {
      accessorKey: "address",
      header: "Address",
      size: 150,
    },
    {
      accessorKey: "city",
      header: "City",
      size: 150,
    },
    {
      accessorKey: "phone",
      header: "Phone",
      size: 150,
    },

    {
      accessorKey: "postal_code",
      header: "Postal Code",
      size: 150,
    },
    {
      accessorKey: "seats",
      header: "Seats",
      size: 150,
    },
    {
      accessorKey: "package.title",
      header: "Package",
      size: 150,
      Cell: ({ row }) => {
        return (
          <Link
            style={{
              fontWeight: "bold",
              textDecoration: "underline",
            }}
            to={`/dashboard/package/${row.original.package.id}`}
          >
            {row.original.package.title}
          </Link>
        );
      },
    },
    {
      accessorKey: "date",
      header: "Package Date",
      size: 200,
      Cell: ({ cell }) => {
        return (
          <span style={{ fontSize: "0.8rem" }}>
            <span style={{ fontWeight: "bold", textDecoration: "underline" }}>
              {cell.getValue()?.from_date}
            </span>{" "}
            to{" "}
            <span style={{ fontWeight: "bold", textDecoration: "underline" }}>
              {cell.getValue()?.to_date}
            </span>
          </span>
        );
      },
    },
    {
      accessorKey: "date_of_registration",
      header: "Register at",
      size: 180,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },
    {
      accessorKey: "contract",
      header: "Contract",
      size: 150,
      Cell: ({ row }) => {
        return (
          <Tooltip sx={{ margin: "0 auto" }} title="Download">
            <IconButton
              onClick={() => {
                downloadFile(
                  `${BASE_URL}/contract/${row.original.id}`,
                  `${row.original.first_name} ${row.original.last_name}`
                );
              }}
            >
              <CloudDownloadIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      size: 180,
      Cell: ({ cell }) => {
        const status = cell.getValue();
        return (
          <span
            style={{
              background:
                status == "pending"
                  ? "#f29339"
                  : status == "confirmed"
                  ? "green"
                  : "#da0600",
              color: "white",
              borderRadius: "4px",
              padding: "0.15rem 0.3rem",
            }}
          >
            {cell.getValue()}
          </span>
        );
      },
    },
    {
      accessorKey: "name",
      header: "Actions",
      size: 250,
      Cell: ({ row }) => {
        if (row.original.status != "pending") return;
        return (
          <div>
            <button
              style={{
                background:
                  confirmIsLoading || rejectIsLoading
                    ? "rgb(174 174 174)"
                    : "#00852e",
                marginRight: "0.8rem",

                color:
                  confirmIsLoading || rejectIsLoading ? "#737373" : "white",
                borderRadius: "4px",
                padding: "0.15rem 0.3rem",
                cursor: "pointer",
                cursor:
                  confirmIsLoading || rejectIsLoading ? "auto" : "pointer",

                outline: "none",
                border: "none",
                cursor:
                  confirmIsLoading || rejectIsLoading ? "auto" : "pointer",
              }}
              disabled={confirmIsLoading || rejectIsLoading}
              onClick={() => {
                confirmRegistration(row.original.id).then(() => {
                  refetch();
                });
              }}
            >
              confirm
            </button>
            <button
              style={{
                background:
                  confirmIsLoading || rejectIsLoading
                    ? "rgb(174 174 174)"
                    : "#da0600",

                color:
                  confirmIsLoading || rejectIsLoading ? "#737373" : "white",
                borderRadius: "4px",
                padding: "0.15rem 0.3rem",
                outline: "none",
                border: "none",
                cursor:
                  confirmIsLoading || rejectIsLoading ? "auto" : "pointer",
              }}
              disabled={confirmIsLoading || rejectIsLoading}
              onClick={() => {
                rejectRegistration(row.original.id).then(() => {
                  refetch();
                });
              }}
            >
              reject
            </button>
          </div>
        );
      },
    },
  ];
  const columns_array_course = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "first_name",
      header: "First Name",
      size: 150,
    },
    {
      accessorKey: "last_name",
      header: "Last Name",
      size: 150,
    },
    {
      accessorKey: "email",
      header: "Email",
      size: 150,
    },
    {
      accessorKey: "address",
      header: "Address",
      size: 150,
    },
    {
      accessorKey: "city",
      header: "City",
      size: 150,
    },
    {
      accessorKey: "phone",
      header: "Phone",
      size: 150,
    },

    {
      accessorKey: "postal_code",
      header: "Postal Code",
      size: 150,
    },
    {
      accessorKey: "seats",
      header: "Seats",
      size: 150,
    },
    {
      accessorKey: "course.title",
      header: "Course",
      size: 150,
      Cell: ({ row }) => {
        return (
          <Link
            style={{
              fontWeight: "bold",
              textDecoration: "underline",
            }}
            to={`/dashboard/course/${row.original.course.id}`}
          >
            {row.original.course.title}
          </Link>
        );
      },
    },
    {
      accessorKey: "date",
      header: "Course Date",
      size: 200,
      Cell: ({ cell }) => {
        return (
          <span style={{ fontSize: "0.8rem" }}>
            <span style={{ fontWeight: "bold", textDecoration: "underline" }}>
              {cell.getValue()?.from_date}
            </span>{" "}
            to{" "}
            <span style={{ fontWeight: "bold", textDecoration: "underline" }}>
              {cell.getValue()?.to_date}
            </span>
          </span>
        );
      },
    },
    {
      accessorKey: "date_of_registration",
      header: "Register at",
      size: 180,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },
    {
      accessorKey: "contract",
      header: "Contract",
      size: 150,
      Cell: ({ row }) => {
        return (
          <Tooltip sx={{ margin: "0 auto" }} title="Download">
            <IconButton
              onClick={() => {
                downloadFile(
                  `${BASE_URL}/contract/${row.original.id}`,
                  `${row.original.first_name} ${row.original.last_name}`
                );
              }}
            >
              <CloudDownloadIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
    {
      accessorKey: "status",
      header: "Status",
      size: 180,
      Cell: ({ cell }) => {
        const status = cell.getValue();
        return (
          <span
            style={{
              background:
                status == "pending"
                  ? "#f29339"
                  : status == "confirmed"
                  ? "green"
                  : "#da0600",
              color: "white",
              borderRadius: "4px",
              padding: "0.15rem 0.3rem",
            }}
          >
            {cell.getValue()}
          </span>
        );
      },
    },
    {
      accessorKey: "name",
      header: "Actions",
      size: 250,
      Cell: ({ row }) => {
        if (row.original.status != "pending") return;
        return (
          <div>
            <button
              style={{
                background:
                  confirmIsLoading || rejectIsLoading
                    ? "rgb(174 174 174)"
                    : "#00852e",
                marginRight: "0.8rem",

                color:
                  confirmIsLoading || rejectIsLoading ? "#737373" : "white",
                borderRadius: "4px",
                padding: "0.15rem 0.3rem",
                cursor:
                  confirmIsLoading || rejectIsLoading ? "auto" : "pointer",
                outline: "none",
                border: "none",
              }}
              disabled={confirmIsLoading || rejectIsLoading}
              onClick={() => {
                confirmRegistration(row.original.id).then(() => {
                  refetch();
                });
              }}
            >
              confirm
            </button>
            <button
              style={{
                background:
                  confirmIsLoading || rejectIsLoading
                    ? "rgb(174 174 174)"
                    : "#da0600",

                color:
                  confirmIsLoading || rejectIsLoading ? "#737373" : "white",
                borderRadius: "4px",
                padding: "0.15rem 0.3rem",
                outline: "none",
                border: "none",
                cursor:
                  confirmIsLoading || rejectIsLoading ? "auto" : "pointer",
              }}
              disabled={confirmIsLoading || rejectIsLoading}
              onClick={() => {
                rejectRegistration(row.original.id).then(() => {
                  refetch();
                });
              }}
            >
              reject
            </button>
          </div>
        );
      },
    },
  ];

  return (
    <Box
      sx={{
        padding: "1.5rem",
      }}
    >
      <Typography
        sx={{
          fontWeight: "bold",
          fontSize: "1.5rem",
          color: ThemeColor.main,
        }}
      >
        Registration
      </Typography>
      {isLoading ? (
        <Loader />
      ) : (
        <Stack p={"0.4rem"} mt={"0.5rem"} gap="1rem">
          <Box>
            <Typography
              sx={{
                fontWeight: "bold",
                fontSize: "1rem",
                color: ThemeColor.main,
                marginBottom: "0.5rem",
              }}
            >
              Packages
            </Typography>
            <Table
              columns_array={columns_array_package}
              data={data?.package_registration}
            />
          </Box>
          <Box>
            <Typography
              sx={{
                fontWeight: "bold",
                fontSize: "1rem",
                color: ThemeColor.main,
                marginBottom: "0.5rem",
              }}
            >
              Courses
            </Typography>
            <Table
              columns_array={columns_array_course}
              data={data?.course_registration}
            />
          </Box>
        </Stack>
      )}
    </Box>
  );
};

export default Registration;
