import { Box, FormControl, Stack, TextField, Typography } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { useGetCategoryQuery } from "../Redux/API/CategoryAPI";
import Loader from "../UI/Loader";
import { ThemeColor } from "../Theme/Theme";
import Table from "../UI/Table";
import Button from "../UI/Button";
import PopupLayout from "../Layout/PopupLayout";
import { IconButton, Tooltip } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import VisibilityIcon from "@mui/icons-material/Visibility";
import {
  useCreateCourseMutation,
  useEditCourseMutation,
  useGetCourseQuery,
  useSwitchActivityMutation,
} from "../Redux/API/CourseAPI";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import UploadPhoto from "../UI/UploadPhoto";
import debounce from "lodash.debounce";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { openNotification } from "../Redux/Slices/NotificationSlice";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Courses = () => {
  const [selectedCategories, setSelectedCategories] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectedCategories(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const columns_array = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "title",
      header: "Name",
      size: 150,
    },
    {
      accessorKey: "address",
      header: "Address",
      size: 150,
    },
    {
      accessorKey: "price",
      header: "Price",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue())?.toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "discount",
      header: "Discount",
      size: 150,
      Cell: ({ cell }) => {
        return <span>{Number(cell.getValue()).toFixed(2)}%</span>;
      },
    },
    {
      accessorKey: "price_after_discount",
      header: "Price After Discount",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue())?.toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "created_at",
      header: "Created At",
      size: 150,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },

    {
      accessorKey: "actions",
      header: "Actions",
      size: 250,

      Cell: ({ row }) => {
        return (
          <div>
            <Tooltip title="Edit">
              <IconButton
                onClick={() => {
                  setEditCourseInfo({
                    id: row.original.id,
                    price: row.original.price,
                    discount: row.original.discount,
                    src: row.original.src,
                    address: row.original.address,
                    title: row.original.title,
                  });

                  openEditPopup();
                }}
                aria-label="edit"
              >
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="show course details">
              <Link
                style={{
                  color: "#757575",
                  margin: "0",
                  padding: "0",
                }}
                to={`/dashboard/course/${row.original.id}`}
              >
                <IconButton to={`/cours/${row.original.id}`} aria-label="show">
                  <VisibilityIcon />
                </IconButton>
              </Link>
            </Tooltip>

            <Tooltip title="click to switch">
              <IconButton
                onClick={(e) => {
                  switchActivity(row.original.id)
                    .then((resp) => {
                      console.log("hello");
                      refetch();
                    })
                    .catch(() => {
                      console.log("error");
                    });
                }}
                disabled={loadingActive}
                aria-label="show"
              >
                <button
                  disabled={loadingActive}
                  style={{
                    background: loadingActive
                      ? "#e5e5e5"
                      : row.original.active == 1
                      ? "#037403"
                      : "#ca0606",
                    padding: "0.3rem 0.5rem",
                    border: "none",
                    outline: "none",
                    color: loadingActive ? "#bdbdbd" : "#eee",
                    borderRadius: "3px",
                    transition: "0.3s",
                    cursor: "pointer",
                    marginLeft: "0.3rem",
                    fontSize: "0.7rem",
                  }}
                >
                  {row.original.active == 1 ? (
                    <span>Active</span>
                  ) : (
                    <span>Not Active</span>
                  )}
                </button>
              </IconButton>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  const [
    switchActivity,
    {
      isLoading: loadingActive,
      isSuccess: isSuccessActive,
      isError: isErrorActive,
    },
  ] = useSwitchActivityMutation();
  const { data, isLoading, isError, error, refetch, isFetching } =
    useGetCourseQuery();
  const { data: categories, isLoading: loadingCategories } =
    useGetCategoryQuery();

  const [
    createCourse,
    {
      data: createData,
      isLoading: createLoading,
      error: createError,
      isSuccess: createIsSuccess,
    },
  ] = useCreateCourseMutation();

  const [
    editCourse,
    {
      data: editData,
      isLoading: editLoading,
      error: editError,
      isSuccess: editIsSuccess,
    },
  ] = useEditCourseMutation();

  const [popupState, setPopupState] = useState(false);

  const [courseInfo, setCourseInfo] = useState({
    title: "",
    discount: 0,
    address: "",
    price: 1,
    src: null,
    categories: [],
  });
  const dispatch = useDispatch();

  useEffect(() => {
    if (createIsSuccess) {
      refetch();
      closePopup();
      dispatch(
        openNotification({
          message: "Course Added Successfully",
          status: "success",
        })
      );
    }
  }, [createIsSuccess]);

  const closePopup = () => {
    setPopupState(false);
  };
  const openPopup = () => {
    setPopupState(true);
  };

  const [errorMessage, setErrorMessage] = useState({ src: false, cat: false });

  const handleCreate = (e) => {
    e.preventDefault();

    if (selectedCategories.length == 0) {
      setErrorMessage({ ...errorMessage, cat: true });
      return;
    }
    if (courseInfo.src == null) {
      setErrorMessage({ ...errorMessage, src: true });
      return;
    }

    setErrorMessage({ src: false, cat: false });

    createCourse({ ...courseInfo, categories: selectedCategories }).then(
      (res) => {
        if (res?.error) {
          if (res.error.status) {
            dispatch(
              openNotification({
                message: "Network Error , Try Again",
                status: "error",
              })
            );
          } else
            dispatch(
              openNotification({ message: "Faild, Try Again", status: "error" })
            );
        }
      }
    );
  };

  //Start Edit
  const [editPopupState, setEditPopupState] = useState(false);
  const [editCourseInfo, setEditCourseInfo] = useState({
    id: 0,
    title: "",
    discount: 0,
    price: 1,
    categories: [],
    src: null,
  });

  const closeEditPopup = () => {
    setEditPopupState(false);
  };
  const openEditPopup = () => {
    setEditPopupState(true);
  };
  const handleEdit = (e) => {
    e.preventDefault();
    editCourse({ ...editCourseInfo }).then((res) => {
      refetch();
      closeEditPopup();
      if (res?.error) {
        if (res.error.status) {
          dispatch(
            openNotification({
              message: "Network Error , Try Again",
              status: "error",
            })
          );
        } else
          dispatch(
            openNotification({ message: "Faild, Try Again", status: "error" })
          );
      } else
        dispatch(
          openNotification({
            message: "Course Edited Successfully",
            status: "success",
          })
        );
    });
  };

  const debouncedSetEditCourseInfo = debounce((newFormData) => {
    setEditCourseInfo(newFormData);
  }, 300);

  const handleEditFormChange = (e) => {
    const { name, value } = e.target;
    const newFormData = {
      ...editCourseInfo,
      [name]: value,
    };
    debouncedSetEditCourseInfo(newFormData);
  };

  // End Edit

  const debouncedSetFormData = debounce((newFormData) => {
    setCourseInfo(newFormData);
  }, 300);

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    const newFormData = {
      ...courseInfo,
      [name]: value,
    };
    debouncedSetFormData(newFormData);
  };

  const handleUpladPic = (image) => {
    setCourseInfo((prev) => {
      return { ...prev, src: image };
    });
  };

  if (isLoading) {
    return <Loader />;
  }

  console.log(editCourseInfo);
  return (
    <Box
      sx={{
        padding: "1.5rem",
      }}
    >
      {popupState && (
        <PopupLayout title={"Add Course"} closePopup={closePopup}>
          {createLoading || loadingCategories ? (
            <Loader />
          ) : categories?.categories?.length === 0 ? (
            <Typography>Please Add Category First</Typography>
          ) : (
            <form onSubmit={handleCreate}>
              <Stack gap={"0.8rem"}>
                <TextField
                  name="title"
                  onChange={handleFormChange}
                  required
                  size="small"
                  label="Title"
                />
                <TextField
                  name="address"
                  onChange={handleFormChange}
                  required
                  size="small"
                  label="Address"
                />
                <TextField
                  name="price"
                  onChange={handleFormChange}
                  required
                  size="small"
                  type="number"
                  label="price"
                  inputProps={{ min: 1 }}
                />
                <TextField
                  name="discount"
                  onChange={handleFormChange}
                  required
                  size="small"
                  type="number"
                  label="discount"
                  inputProps={{ min: 0, max: 100 }}
                />

                <FormControl>
                  <InputLabel id="demo-multiple-chip-label">
                    Categories
                  </InputLabel>
                  <Select
                    labelId="demo-multiple-chip-label"
                    id="demo-multiple-chip"
                    multiple
                    value={selectedCategories}
                    onChange={handleChange}
                    style={{}}
                    input={
                      <OutlinedInput
                        id="select-multiple-chip"
                        label="Categories"
                      />
                    }
                    renderValue={(selected) => (
                      <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                        {selected.map((value) => (
                          <Chip
                            key={value}
                            label={
                              categories.categories.find(
                                (item) => item.id == value
                              ).name
                            }
                          />
                        ))}
                      </Box>
                    )}
                    MenuProps={MenuProps}
                  >
                    {categories?.categories?.map((cat) => (
                      <MenuItem
                        key={cat.id}
                        value={cat.id}
                        //   style={getStyles(name, personName, theme)}
                        style={{ zIndex: "10000" }}
                      >
                        {cat.name}
                      </MenuItem>
                    ))}
                  </Select>
                  {errorMessage.cat && (
                    <Typography color="error">
                      please select category
                    </Typography>
                  )}
                  <UploadPhoto handleUpladPic={handleUpladPic} />
                </FormControl>
                {errorMessage.src && (
                  <Typography color="error">please add image</Typography>
                )}
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Create</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {editPopupState && (
        <PopupLayout title={"Edit Course"} closePopup={closeEditPopup}>
          {editLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleEdit}>
              <Stack gap={"0.8rem"}>
                <TextField
                  name="title"
                  onChange={handleEditFormChange}
                  required
                  defaultValue={editCourseInfo.title}
                  size="small"
                  label="Title"
                />
                <TextField
                  name="address"
                  onChange={handleEditFormChange}
                  defaultValue={editCourseInfo.address}
                  required
                  size="small"
                  label="Address"
                />
                <TextField
                  name="price"
                  onChange={handleEditFormChange}
                  defaultValue={editCourseInfo.price}
                  required
                  size="small"
                  type="number"
                  label="price"
                  inputProps={{ min: 1 }}
                />
                <TextField
                  name="discount"
                  onChange={handleEditFormChange}
                  defaultValue={editCourseInfo.discount}
                  required
                  size="small"
                  type="number"
                  label="discount"
                  inputProps={{ min: 0, max: 100 }}
                />
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Edit</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      <Stack
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        mb={"1rem"}
      >
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.5rem",
            color: ThemeColor.main,
          }}
        >
          Courses
        </Typography>
        <Button onClick={openPopup}>Add course</Button>
      </Stack>
      <Table columns_array={columns_array} data={data?.courses} />
    </Box>
  );
};

export default Courses;
