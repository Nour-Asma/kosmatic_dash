import { Box, Stack, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import { ThemeColor } from "../../Theme/Theme";
import Button from "../../UI/Button";
import CourseSection from "./CourseSection";

const CourseSections = ({ courseSectionsData, openPopup, refetch }) => {
  return (
    <div
    //   style={{
    //     maxHeight: "100vh",
    //     overflowY: "auto",
    //   }}
    >
      <Stack
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        mb={"1rem"}
      >
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.5rem",
            color: ThemeColor.main,
          }}
        >
          Course Sections
        </Typography>
        <Button onClick={openPopup}>Add course section</Button>
      </Stack>
      {courseSectionsData?.length == 0 ? (
        <Typography
          sx={{ textAlign: "center", color: "gray", fontWeight: "bold" }}
        >
          No sections
        </Typography>
      ) : (
        <Stack
          gap="0.6rem"
          sx={{
            margin: "1rem 0 ",
            padding: "1rem",
            border: `1px solid ${ThemeColor.secondary}`,
            borderRadius: "3px",
          }}
        >
          {courseSectionsData?.map((item, index) => {
            return (
              <CourseSection
                refetch={refetch}
                key={index}
                courseSectionData={item}
              />
            );
          })}
        </Stack>
      )}
    </div>
  );
};

export default CourseSections;
