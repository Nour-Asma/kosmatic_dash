import React, { useState } from "react";
import {
  Box,
  IconButton,
  Stack,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";

import { BASE_URL_IMAGE } from "../../data/BaseURL";
import DeleteIcon from "@mui/icons-material/Delete";
import Loader from "../../UI/Loader";
import PopupLayout from "../../Layout/PopupLayout";
import Button from "../../UI/Button";
import {
  useDeleteCourseSectionMutation,
  useUpdateCourseSectionMutation,
} from "../../Redux/API/CourseAPI";
import EditIcon from "@mui/icons-material/Edit";
import debounce from "lodash.debounce";
import UploadPhoto from "../../UI/UploadPhoto";

const CourseSection = ({ courseSectionData, refetch }) => {
  const [popupState, setPopupState] = useState(false);
  const closePopup = () => {
    setPopupState(false);
  };
  const openPopup = () => {
    setPopupState(true);
  };

  const [sectionToEdit, setSectionToEdit] = useState({
    course_id: courseSectionData.id,
    title: null,
    information: null,
    src: null,
  });

  const [popupEditState, setPopupEditState] = useState(false);
  const closePopupEdit = () => {
    setPopupEditState(false);
  };
  const openEditPopup = () => {
    setPopupEditState(true);
  };

  const [deleteCourseSection, { isLoading: deleteIsLoading }] =
    useDeleteCourseSectionMutation();

  const [updateCourseSection, { isLoading: updateIsLoading }] =
    useUpdateCourseSectionMutation();

  const deleteSection = () => {
    deleteCourseSection(courseSectionData.id).then(() => {
      closePopup();
      refetch();
    });
  };

  console.log(updateIsLoading);
  const updateSection = (e) => {
    e.preventDefault();
    updateCourseSection(sectionToEdit).then(() => {
      closePopupEdit();
      refetch();
    });
  };

  const handleUpladPic = (image) => {
    setSectionToEdit((prev) => {
      return { ...prev, src: image };
    });
  };

  const debouncedSetFormData = debounce((newFormData) => {
    setSectionToEdit(newFormData);
  }, 300);

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    const newFormData = {
      ...sectionToEdit,
      [name]: value,
    };
    debouncedSetFormData(newFormData);
  };

  const handleCreate = () => {
    // setSectionToEdit(sectionToEdit).then(() => {
    //   refetch();
    //   closePopup();
    // });
  };

  return (
    <Stack gap="0.4rem">
      {popupState && (
        <PopupLayout title={"Confirmation"} closePopup={closePopup}>
          {deleteIsLoading ? (
            <Loader />
          ) : (
            <>
              <Typography>
                Are you sure you want to delete this section
              </Typography>
              <Stack
                direction={"row"}
                justifyContent={"center"}
                alignItems={"center"}
                gap={"2rem"}
                mt={"2rem"}
              >
                <Button onClick={deleteSection}>OK</Button>
                <button
                  style={{
                    fontSize: "1rem",
                    padding: "0.5rem 1rem",
                    color: "white",
                    outline: "none",
                    borderRadius: "5px",
                    border: "none",
                    cursor: "pointer",
                    background: "rgb(202, 6, 6)",
                  }}
                  onClick={closePopup}
                >
                  Cancel
                </button>
              </Stack>
            </>
          )}
        </PopupLayout>
      )}

      {popupEditState && (
        <PopupLayout title={"Add new section"} closePopup={closePopupEdit}>
          {updateIsLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleCreate}>
              <Stack gap={"0.8rem"}>
                <TextField
                  onChange={handleFormChange}
                  size="small"
                  label="title"
                  name="title"
                  defaultValue={courseSectionData.title}
                />
                <TextField
                  multiline
                  onChange={handleFormChange}
                  size="small"
                  label="information"
                  name="information"
                  defaultValue={courseSectionData.information}
                />
                <UploadPhoto
                  defaultImage={`${BASE_URL_IMAGE}${courseSectionData.src}`}
                  handleUpladPic={handleUpladPic}
                />
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button onClick={updateSection}>Update</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}
      <Stack direction={"row"} gap={"1rem"} alignItems={"center"}>
        <Typography sx={{ fontWeight: "bold" }}>
          {courseSectionData.title}
        </Typography>
        <Tooltip title="Delete">
          <IconButton onClick={openPopup} aria-label="Delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Edit">
          <IconButton onClick={openEditPopup} aria-label="Edit">
            <EditIcon />
          </IconButton>
        </Tooltip>
      </Stack>
      {courseSectionData.src && (
        <Box sx={{ maxWidth: "250px" }}>
          <img
            style={{ maxWidth: "100%" }}
            src={`${BASE_URL_IMAGE}${courseSectionData.src}`}
          />
        </Box>
      )}
      <Typography>{courseSectionData.information}</Typography>
    </Stack>
  );
};

export default CourseSection;
