import {
  Box,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { ThemeColor } from "../../Theme/Theme";
import { useNavigate, useParams } from "react-router-dom";
import {
  useAddCourseCategoryMutation,
  useCreateCourseSectionMutation,
  useDeleteCourseCategoryMutation,
  useEditCoursePicMutation,
  useGetCourseDetailsQuery,
} from "../../Redux/API/CourseAPI";
import Loader from "../../UI/Loader";
import InfoPair from "../../UI/InfoPair";
import { BASE_URL_IMAGE } from "../../data/BaseURL";
import CourseSections from "./CourseSections";
import PopupLayout from "../../Layout/PopupLayout";
import Button from "../../UI/Button";
import UploadPhoto from "../../UI/UploadPhoto";
import debounce from "lodash.debounce";
import EditIcon from "@mui/icons-material/Edit";
import DateUI from "../../UI/DateUI";
import SmallButton from "../../UI/SmallButton";
import { useGetCategoryQuery } from "../../Redux/API/CategoryAPI";
import { useDispatch } from "react-redux";
import { openNotification } from "../../Redux/Slices/NotificationSlice";

const CourseDetails = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [sectionToCreate, setSectionToCreate] = useState({
    course_id: id,
    title: "",
    information: "",
    src: null,
  });

  const [
    createCourseSection,
    { data: createSectionData, isLoading: createSectionIsLoading },
  ] = useCreateCourseSectionMutation();

  const {
    data: courseData,
    isLoading: isLoadingCourseData,
    isSuccess,
    refetch,
  } = useGetCourseDetailsQuery(id);

  const [popupState, setPopupState] = useState(false);
  const closePopup = () => {
    setPopupState(false);
    setSectionToCreate({
      course_id: id,
      title: "",
      information: "",
      src: null,
    });
  };
  const openPopup = () => {
    setPopupState(true);
  };

  // Start Edit Pic

  const [newImage, setNewImage] = useState({ src: null, id: 0 });

  useEffect(() => {
    if (isSuccess) {
      setNewImage({ src: courseData.course.src, id: courseData.course.id });
    }
  }, [isSuccess]);

  const [editPicPopup, setEditPicPopup] = useState(false);
  const closeEditPicPopup = () => {
    setEditPicPopup(false);
  };
  const openEditPicPopup = () => {
    setEditPicPopup(true);
  };

  const handleEditPic = (image) => {
    setNewImage((prev) => {
      return { ...prev, src: image };
    });
  };

  const editPicSubmitHandler = (e) => {
    e.preventDefault();
    editCoursePic(newImage).then(() => {
      closeEditPicPopup();
    });
  };

  const [editCoursePic, { isLoading: isLoadingEditPic }] =
    useEditCoursePicMutation();

  // End Edit Pic

  const handleUpladPic = (image) => {
    setSectionToCreate((prev) => {
      return { ...prev, src: image };
    });
  };

  const debouncedSetFormData = debounce((newFormData) => {
    setSectionToCreate(newFormData);
  }, 300);

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    const newFormData = {
      ...sectionToCreate,
      [name]: value,
    };
    debouncedSetFormData(newFormData);
  };

  const dispatch = useDispatch();
  const handleCreate = () => {
    createCourseSection(sectionToCreate).then((res) => {
      refetch();
      closePopup();

      if (res?.error) {
        if (res.error.status) {
          dispatch(
            openNotification({
              message: "Network Error , Try Again",
              status: "error",
            })
          );
        } else
          dispatch(
            openNotification({ message: "Faild, Try Again", status: "error" })
          );
      } else {
        dispatch(
          openNotification({
            message: "Section Added Successfully",
            status: "success",
          })
        );
      }
    });
  };
  // START DELETE CATEGORY
  const { data: categories, isLoading: loadingCategories } =
    useGetCategoryQuery();

  const [deleteCourseCategory, { isLoading: loadingDeleteCategory }] =
    useDeleteCourseCategoryMutation();

  const handleDeleteCategory = (category_id) => {
    deleteCourseCategory({
      category_id,
      course_id: courseData.course.id,
    }).then(() => {
      refetch();
    });
  };

  // End DELETE CATEGORY

  //START ADD TO CATEGORY
  const [addToCatPopup, setAddToCatPopup] = useState();
  const closeAddToCatPopup = () => {
    setAddToCatPopup(false);
  };
  const openAddToCatPopup = () => {
    setAddToCatPopup(true);
  };

  const [addToCategory, setAddToCategory] = useState(null);
  const handleAddToCatChange = (e) => {
    setAddToCategory(e.target.value);
  };

  const addToCategorySubmitHandler = (e) => {
    e.preventDefault();
    addCourseCategory({
      course_id: courseData.course.id,
      category_id: addToCategory,
    }).then(() => {
      refetch();
      closeAddToCatPopup();
      setAddToCategory(null);
    });
  };

  const [addCourseCategory, { isLoading: addToCategoryLoading }] =
    useAddCourseCategoryMutation();

  //END ADD TO CATEGORY
  console.log(`${BASE_URL_IMAGE}${courseData?.course?.src}`);

  return (
    <Box sx={{ padding: "1rem" }}>
      {popupState && (
        <PopupLayout title={"Add new section"} closePopup={closePopup}>
          {createSectionIsLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleCreate}>
              <Stack gap={"0.8rem"}>
                <TextField
                  onChange={handleFormChange}
                  size="small"
                  label="title"
                  name="title"
                />
                <TextField
                  multiline
                  onChange={handleFormChange}
                  size="small"
                  label="information"
                  name="information"
                />
                <UploadPhoto handleUpladPic={handleUpladPic} />
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Create</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {editPicPopup && (
        <PopupLayout title={"Edit Course Pic"} closePopup={closeEditPicPopup}>
          {isLoadingEditPic ? (
            <Loader />
          ) : (
            <form onSubmit={editPicSubmitHandler}>
              <Stack gap={"0.8rem"}>
                <UploadPhoto handleUpladPic={handleEditPic} />
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Edit</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {addToCatPopup && (
        <PopupLayout title={"Add To Category"} closePopup={closeAddToCatPopup}>
          {loadingCategories || addToCategoryLoading ? (
            <Loader />
          ) : (
            <form onSubmit={addToCategorySubmitHandler}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Categories
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={addToCategory}
                  label="Categories"
                  onChange={handleAddToCatChange}
                >
                  {categories.categories.map((item) => {
                    const show = courseData.course.categories.find(
                      (cc) => cc.id == item.id
                    );

                    if (!show)
                      return <MenuItem value={item.id}>{item.name}</MenuItem>;
                  })}
                </Select>
              </FormControl>
              <div style={{ margin: "1rem 0" }}>
                <Button disabled={addToCategory == null} type="submit">
                  Add
                </Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      <Typography
        sx={{
          fontWeight: "bold",
          fontSize: "1.5rem",
          color: ThemeColor.main,
        }}
      >
        Course Details
      </Typography>
      {isLoadingCourseData ? (
        <Loader />
      ) : (
        <Box>
          <Stack
            sx={{
              margin: "1rem 0 ",
              padding: "1rem",
              border: `1px solid ${ThemeColor.secondary}`,
              borderRadius: "3px",
            }}
            direction={{
              md: "row",
              sm: "column-reverse",
              xs: "column-reverse",
            }}
            gap="1rem"
            justifyContent={"space-between"}
          >
            <Stack gap={"1rem"} sx={{ flexBasis: "50%" }}>
              <InfoPair title={"Id"} info={courseData.course.id} />
              <InfoPair title={"Title"} info={courseData.course.title} />
              <InfoPair title={"Address"} info={courseData.course.address} />
              <InfoPair
                title={"Price"}
                info={
                  <span>
                    {Number(courseData.course.price).toFixed(2)}{" "}
                    <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>
                      EURO
                    </span>
                  </span>
                }
              />
              <InfoPair
                title={"Price After Discount"}
                info={
                  <span>
                    {Number(courseData.course.price_after_discount).toFixed(2)}{" "}
                    <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>
                      EURO
                    </span>
                  </span>
                }
              />
              <InfoPair
                title={"Active"}
                info={
                  <span>
                    {courseData.course.active ? (
                      <span
                        style={{
                          fontSize: "0.8rem",
                          border: "1px solid green",
                          padding: "0.1rem 0.4rem",
                          borderRadius: "3px",
                          color: "white",
                          background: "green",
                          marginTop: "-10px",
                        }}
                      >
                        Active
                      </span>
                    ) : (
                      <span
                        style={{
                          fontSize: "0.8rem",
                          border: "1px solid rgb(202, 6, 6)",
                          padding: "0.1rem 0.4rem",
                          borderRadius: "3px",
                          color: "white",
                          background: "rgb(202, 6, 6)",
                          marginTop: "-10px",
                        }}
                      >
                        Not active
                      </span>
                    )}
                  </span>
                }
              />
              <InfoPair
                title={"Belongs to categories"}
                info={
                  <Box>
                    {courseData.course.categories.map((item) => {
                      return (
                        <Chip
                          onDelete={() => {
                            handleDeleteCategory(item.id);
                          }}
                          disabled={loadingDeleteCategory}
                          key={item.id}
                          sx={{ margin: "0.4rem" }}
                          label={item.name}
                        />
                      );
                    })}
                    <SmallButton onClick={openAddToCatPopup}>
                      Add to category
                    </SmallButton>
                  </Box>
                }
              />
              <InfoPair
                title={"Belongs to packages"}
                info={courseData.course.part_of_packages.map((item) => (
                  <Chip
                    key={item.id}
                    label={item.title}
                    onClick={() => {
                      navigate("/dashboard/package/" + item.id);
                    }}
                    sx={{
                      cursor: "pointer",
                      margin: "0.4rem .4rem",
                      borderRadius: "4px",
                      textDecoration: "underline",
                      fontWeight: "bold",
                      "&:hover": {
                        background: "#a29e9e",
                      },
                    }}
                  />
                ))}
              />
            </Stack>
            <Box
              sx={{
                flexBasis: "50%",
                maxHeight: { sm: "100%" },
                transition: "0.2s",
                boxShadow: "0 0 0 0 black",
                "&:hover": {},
              }}
            >
              <div className="courseImage">
                <div
                  onClick={openEditPicPopup}
                  className="editIconForCourseImage"
                >
                  <EditIcon sx={{ transform: "scale(2.5)" }} />
                </div>
                <img
                  loading="lazy"
                  style={{
                    maxWidth: "100%",
                    maxHeight: "100%",
                    margin: "0 0 0 auto ",
                    display: "block",
                  }}
                  src={`${BASE_URL_IMAGE}${courseData.course.src}`}
                />
              </div>
            </Box>
          </Stack>
          <Box>
            <Typography
              sx={{
                fontWeight: "bold",
                fontSize: "1.5rem",
                color: ThemeColor.main,
              }}
            >
              Course Dates
            </Typography>

            <Stack
              sx={{
                margin: "1rem 0 ",
                padding: "1rem",
                border: `1px solid ${ThemeColor.secondary}`,
                borderRadius: "3px",
              }}
              gap="1rem"
              justifyContent={"space-between"}
            >
              <DateUI
                refetch={refetch}
                course_id={courseData.course.id}
                datesArray={courseData.course.dates}
              />
            </Stack>
          </Box>
          <CourseSections
            openPopup={openPopup}
            courseSectionsData={courseData.sections}
            refetch={refetch}
          />
        </Box>
      )}
    </Box>
  );
};

export default CourseDetails;
