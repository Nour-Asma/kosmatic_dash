import { Box, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import { ThemeColor } from "../../Theme/Theme";
import Button from "../../UI/Button";
import { useChangPasswrodMutation } from "../../Redux/API/AuthAPI";
import { useDispatch } from "react-redux";
import { openNotification } from "../../Redux/Slices/NotificationSlice";

const Setting = () => {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState(null);
  const [changePassword, { isLoading }] = useChangPasswrodMutation();

  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    if (password !== confirmPassword) {
      setError("Passwords do not match");
    } else {
      // Handle form submission logic here
      setError("");
      changePassword({
        password: password,
        password_confirmation: confirmPassword,
      }).then((res) => {
        if (res?.error) {
          if (res.error.status) {
            dispatch(
              openNotification({
                message: "Network Error , Try Again",
                status: "error",
              })
            );
          } else
            dispatch(
              openNotification({ message: "Faild, Try Again", status: "error" })
            );
        } else {
          dispatch(
            openNotification({
              message: "Passwrod Changed Successfully",
              status: "success",
            })
          );
        }
      });
    }
  };
  return (
    <Box
      sx={{
        padding: "1rem",
      }}
    >
      <Typography sx={{ fontWeight: "bold", color: ThemeColor.main }}>
        Edit Password
      </Typography>

      <Box
        component="form"
        onSubmit={handleSubmit}
        noValidate
        autoComplete="off"
      >
        <TextField
          margin="normal"
          required
          size="small"
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <TextField
          margin="normal"
          required
          size="small"
          name="confirmPassword"
          label="Confirm Password"
          type="password"
          id="confirmPassword"
          autoComplete="current-password"
          value={confirmPassword}
          sx={{ display: "block" }}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        {error && (
          <Typography
            sx={{ padding: "0.2rem 0", fontWeight: "600", fontSize: "0.8rem" }}
            color="error"
          >
            {error}
          </Typography>
        )}

        <Button disabled={password === "" || isLoading} type="submit">
          Confirm
        </Button>
      </Box>
    </Box>
  );
};

export default Setting;
