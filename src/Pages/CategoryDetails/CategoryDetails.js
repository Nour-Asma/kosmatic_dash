import React from "react";
import { Link, useParams } from "react-router-dom";
import { useGetCategoryDetailsQuery } from "../../Redux/API/CategoryAPI";
import { Box, IconButton, Tooltip, Typography } from "@mui/material";
import { ThemeColor } from "../../Theme/Theme";
import Loader from "../../UI/Loader";
import VisibilityIcon from "@mui/icons-material/Visibility";
import Table from "../../UI/Table";

const CategoryDetails = () => {
  const { id } = useParams();
  const { data, isLoading } = useGetCategoryDetailsQuery(id);

  const columns_array_courses = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "title",
      header: "Name",
      size: 150,
    },
    {
      accessorKey: "address",
      header: "Address",
      size: 150,
    },
    {
      accessorKey: "price",
      header: "Price",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue()).toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "discount",
      header: "Discount",
      size: 150,
      Cell: ({ cell }) => {
        return <span>{Number(cell.getValue())?.toFixed(2)}%</span>;
      },
    },
    {
      accessorKey: "price_after_discount",
      header: "Price After Discount",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue())?.toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "created_at",
      header: "Created At",
      size: 150,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },

    {
      accessorKey: "actions",
      header: "Actions",
      size: 250,

      Cell: ({ row }) => {
        return (
          <div>
            <Tooltip title="show course details">
              <Link
                style={{
                  color: "#757575",
                  margin: "0",
                  padding: "0",
                }}
                to={`/dashboard/course/${row.original.id}`}
              >
                <IconButton to={`/cours/${row.original.id}`} aria-label="show">
                  <VisibilityIcon />
                </IconButton>
              </Link>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  const columns_array_packages = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "title",
      header: "Name",
      size: 150,
    },

    {
      accessorKey: "price",
      header: "Price",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue())?.toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "address",
      header: "Address",
      size: 150,
    },
    {
      accessorKey: "discount",
      header: "Discount",
      size: 150,
      Cell: ({ cell }) => {
        return <span>{Number(cell.getValue())?.toFixed(2)}%</span>;
      },
    },
    {
      accessorKey: "price_after_discount",
      header: "Price After Discount",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue())?.toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "created_at",
      header: "Created At",
      size: 150,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },

    {
      accessorKey: "actions",
      header: "Actions",
      size: 250,

      Cell: ({ row }) => {
        return (
          <div>
            <Tooltip title="show package details">
              <Link
                style={{
                  color: "#757575",
                  margin: "0",
                  padding: "0",
                }}
                to={`/dashboard/package/${row.original.id}`}
              >
                <IconButton aria-label="show">
                  <VisibilityIcon />
                </IconButton>
              </Link>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  return (
    <Box sx={{ padding: "1rem" }}>
      {" "}
      {isLoading ? (
        <Loader />
      ) : (
        <Box>
          <Typography
            sx={{
              fontWeight: "bold",
              fontSize: "1.5rem",
              color: ThemeColor.main,
            }}
          >
            <span style={{ textDecoration: "underline" }}>
              {data?.category?.name}
            </span>{" "}
            Details
          </Typography>

          <Box sx={{ margin: "0.5rem" }}>
            <Box>
              {" "}
              <Typography
                sx={{
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: ThemeColor.main,
                  marginBottom: "0.5rem",
                }}
              >
                Courses
              </Typography>
              <Table
                columns_array={columns_array_courses}
                data={data?.category.courses}
              />
            </Box>
            <Box mt={"1rem"}>
              {" "}
              <Typography
                sx={{
                  fontWeight: "bold",
                  fontSize: "1rem",
                  color: ThemeColor.main,
                  marginBottom: "0.5rem",
                }}
              >
                Packages
              </Typography>
              <Table
                columns_array={columns_array_packages}
                data={data?.category.packages}
              />
            </Box>
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default CategoryDetails;
