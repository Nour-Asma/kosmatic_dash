import {
  Box,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState, useEffect } from "react";
import { ThemeColor } from "../../Theme/Theme";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../UI/Loader";
import InfoPair from "../../UI/InfoPair";
import { BASE_URL_IMAGE } from "../../data/BaseURL";
import {
  useAddCoursePackageMutation,
  useAddPackageCategoryMutation,
  useDeleteCoursePackageMutation,
  useDeletePackageCategoryMutation,
  useEditPackagePicMutation,
  useGetPackageDetailsQuery,
} from "../../Redux/API/PackagesAPI";
import DateUI from "../../UI/DateUI";
import PopupLayout from "../../Layout/PopupLayout";
import UploadPhoto from "../../UI/UploadPhoto";
import Button from "../../UI/Button";
import EditIcon from "@mui/icons-material/Edit";
import {
  useAddCourseCategoryMutation,
  useGetCourseQuery,
} from "../../Redux/API/CourseAPI";
import { useGetCategoryQuery } from "../../Redux/API/CategoryAPI";
import SmallButton from "../../UI/SmallButton";

const PackageDetails = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const {
    data: packageData,
    isLoading: isLoadingData,
    refetch,
    isSuccess,
  } = useGetPackageDetailsQuery(id);

  // Start Edit Pic

  const [newImage, setNewImage] = useState({ src: null, id: 0 });

  useEffect(() => {
    if (isSuccess) {
      setNewImage({ src: packageData.package.src, id: packageData.package.id });
    }
  }, [isSuccess]);

  const [editPicPopup, setEditPicPopup] = useState(false);
  const closeEditPicPopup = () => {
    setEditPicPopup(false);
  };
  const openEditPicPopup = () => {
    setEditPicPopup(true);
  };

  const handleEditPic = (image) => {
    setNewImage((prev) => {
      return { ...prev, src: image };
    });
  };

  const editPicSubmitHandler = (e) => {
    e.preventDefault();
    editPackagePic(newImage).then(() => {
      closeEditPicPopup();
    });
  };

  const [editPackagePic, { isLoading: isLoadingEditPic }] =
    useEditPackagePicMutation();

  // End Edit Pic

  // START DELETE CATEGORY
  const [deletePackageCategory, { isLoading: loadingDeleteCategory }] =
    useDeletePackageCategoryMutation();

  const handleDeleteCategory = (category_id) => {
    deletePackageCategory({
      category_id,
      package_id: packageData.package.id,
    }).then(() => {
      refetch();
    });
  };

  // End DELETE CATEGORY

  //START ADD TO CATEGORY

  const { data: categories, isLoading: loadingCategories } =
    useGetCategoryQuery();

  const [addToCatPopup, setAddToCatPopup] = useState();
  const closeAddToCatPopup = () => {
    setAddToCatPopup(false);
  };
  const openAddToCatPopup = () => {
    setAddToCatPopup(true);
  };

  const [addToCategory, setAddToCategory] = useState(null);
  const handleAddToCatChange = (e) => {
    setAddToCategory(e.target.value);
  };

  const addToCategorySubmitHandler = (e) => {
    e.preventDefault();
    console.log({
      package_id: packageData.package.id,
      category_id: addToCategory,
    });
    addPackageCategory({
      package_id: packageData.package.id,
      category_id: addToCategory,
    }).then(() => {
      refetch();
      closeAddToCatPopup();
      setAddToCategory(null);
    });
  };

  const [addPackageCategory, { isLoading: addToCategoryLoading }] =
    useAddPackageCategoryMutation();

  //END ADD TO CATEGORY

  //START ADD COURSE TO PACKAGE

  const { data: courses, isLoading: loadingCourses } = useGetCourseQuery();

  const [coursePopup, setCoursePopup] = useState();
  const closeCoursePopup = () => {
    setCoursePopup(false);
  };
  const openCoursePopup = () => {
    setCoursePopup(true);
  };

  const [course, setCourse] = useState(null);
  const handleCourseChange = (e) => {
    setCourse(e.target.value);
  };

  const addCourseSubmitHandler = (e) => {
    e.preventDefault();

    addCoursePackage({
      package_id: packageData.package.id,
      course_id: course,
    }).then(() => {
      refetch();
      closeCoursePopup();
      setCourse(null);
    });
  };

  const [addCoursePackage, { isLoading: addCoursePackageLoading }] =
    useAddCoursePackageMutation();

  //END ADD COURSE TO PACKAGE

  //START DELETE COURSE PACKAGE
  const [deleteCoursePackage, { isLoading: deleteCoursePackageLoading }] =
    useDeleteCoursePackageMutation();

  const deleteCoursePackageHandler = (course_id) => {
    deleteCoursePackage({
      course_id,
      package_id: packageData.package.id,
    }).then(() => {
      refetch();
    });
  };
  //END DELETE COURSE PACKAGE

  return (
    <Box sx={{ padding: "1rem" }}>
      <Typography
        sx={{
          fontWeight: "bold",
          fontSize: "1.5rem",
          color: ThemeColor.main,
        }}
      >
        Package Details
      </Typography>

      {editPicPopup && (
        <PopupLayout title={"Edit Package Pic"} closePopup={closeEditPicPopup}>
          {isLoadingEditPic ? (
            <Loader />
          ) : (
            <form onSubmit={editPicSubmitHandler}>
              <Stack gap={"0.8rem"}>
                <UploadPhoto handleUpladPic={handleEditPic} />
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Edit</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {addToCatPopup && (
        <PopupLayout title={"Add To Category"} closePopup={closeAddToCatPopup}>
          {loadingCategories || addToCategoryLoading ? (
            <Loader />
          ) : (
            <form onSubmit={addToCategorySubmitHandler}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Categories
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={addToCategory}
                  label="Categories"
                  onChange={handleAddToCatChange}
                >
                  {categories.categories.map((item) => {
                    const show = packageData.package.categories.find(
                      (cc) => cc.id == item.id
                    );

                    if (!show)
                      return <MenuItem value={item.id}>{item.name}</MenuItem>;
                  })}
                </Select>
              </FormControl>
              <div style={{ margin: "1rem 0" }}>
                <Button disabled={addToCategory == null} type="submit">
                  Add
                </Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {coursePopup && (
        <PopupLayout title={"Add To Category"} closePopup={closeCoursePopup}>
          {loadingCourses || addCoursePackageLoading ? (
            <Loader />
          ) : (
            <form onSubmit={addCourseSubmitHandler}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Courses</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={course}
                  label="Courses"
                  onChange={handleCourseChange}
                >
                  {courses.courses.map((item) => {
                    const show = packageData.package.courses.find(
                      (cc) => cc.id == item.id
                    );

                    if (!show)
                      return <MenuItem value={item.id}>{item.title}</MenuItem>;
                  })}
                </Select>
              </FormControl>
              <div style={{ margin: "1rem 0" }}>
                <Button disabled={course == null} type="submit">
                  Add
                </Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {isLoadingData ? (
        <Loader />
      ) : (
        <Box>
          <Stack
            sx={{
              margin: "1rem 0 ",
              padding: "1rem",
              border: `1px solid ${ThemeColor.secondary}`,
              borderRadius: "3px",
            }}
            direction={{ sm: "row", xs: "column-reverse" }}
            gap="1rem"
            justifyContent={"space-between"}
          >
            <Stack gap={"1rem"} sx={{ flexBasis: "50%" }}>
              <InfoPair title={"Id"} info={packageData.package.id} />
              <InfoPair title={"Title"} info={packageData.package.title} />
              <InfoPair title={"Address"} info={packageData.package.address} />

              <InfoPair
                title={"Price"}
                info={
                  <span>
                    {Number(packageData.package.price).toFixed(2)}{" "}
                    <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>
                      EURO
                    </span>
                  </span>
                }
              />
              <InfoPair
                title={"Price After Discount"}
                info={
                  <span>
                    {Number(packageData.package.price_after_discount).toFixed(
                      2
                    )}{" "}
                    <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>
                      EURO
                    </span>
                  </span>
                }
              />
              <InfoPair
                title={"Active"}
                info={
                  <span>
                    {packageData.package.active ? (
                      <span
                        style={{
                          fontSize: "0.8rem",
                          border: "1px solid green",
                          padding: "0.1rem 0.4rem",
                          borderRadius: "3px",
                          color: "white",
                          background: "green",
                          marginTop: "-10px",
                        }}
                      >
                        Active
                      </span>
                    ) : (
                      <span
                        style={{
                          fontSize: "0.8rem",
                          border: "1px solid rgb(202, 6, 6)",
                          padding: "0.1rem 0.4rem",
                          borderRadius: "3px",
                          color: "white",
                          background: "rgb(202, 6, 6)",
                          marginTop: "-10px",
                        }}
                      >
                        Not active
                      </span>
                    )}
                  </span>
                }
              />
              <InfoPair
                title={"description"}
                info={packageData.package.description}
              />
              <InfoPair
                title={"Belongs to categories"}
                info={
                  <Box>
                    {packageData.package.categories.map((item) => (
                      <Chip
                        onDelete={() => {
                          handleDeleteCategory(item.id);
                        }}
                        disabled={loadingDeleteCategory}
                        key={item.id}
                        sx={{ margin: "0.4rem" }}
                        label={item.name}
                      />
                    ))}
                    <SmallButton onClick={openAddToCatPopup}>
                      Add to category
                    </SmallButton>
                  </Box>
                }
              />
              <InfoPair
                title={"Contains"}
                info={
                  <Box>
                    {packageData.package?.courses?.map((item) => (
                      <Chip
                        onDelete={() => {
                          deleteCoursePackageHandler(item.id);
                        }}
                        disabled={deleteCoursePackageLoading}
                        key={item.id}
                        label={item.title}
                        onClick={() => {
                          navigate("/dashboard/course/" + item.id);
                        }}
                        sx={{
                          cursor: "pointer",
                          margin: "0.4rem .4rem",
                          borderRadius: "4px",
                          textDecoration: "underline",
                          fontWeight: "bold",
                        }}
                      />
                    ))}
                    <SmallButton onClick={openCoursePopup}>
                      Add course to this package
                    </SmallButton>
                  </Box>
                }
              />
            </Stack>

            <Box
              sx={{
                flexBasis: "50%",
                maxHeight: { sm: "100%" },
                transition: "0.2s",
                boxShadow: "0 0 0 0 black",
                "&:hover": {},
              }}
            >
              <div className="courseImage">
                <div
                  onClick={openEditPicPopup}
                  className="editIconForCourseImage"
                >
                  <EditIcon sx={{ transform: "scale(2.5)" }} />
                </div>
                <img
                  loading="lazy"
                  style={{
                    maxWidth: "100%",
                    maxHeight: "100%",
                    margin: "0 0 0 auto ",
                    display: "block",
                  }}
                  src={`${BASE_URL_IMAGE}${packageData.package.image}`}
                />
              </div>
            </Box>
          </Stack>

          <Box>
            <Typography
              sx={{
                fontWeight: "bold",
                fontSize: "1.5rem",
                color: ThemeColor.main,
              }}
            >
              Package Dates
            </Typography>

            <Stack
              sx={{
                margin: "1rem 0 ",
                padding: "1rem",
                border: `1px solid ${ThemeColor.secondary}`,
                borderRadius: "3px",
              }}
              gap="1rem"
              justifyContent={"space-between"}
            >
              <DateUI
                refetch={refetch}
                package_id={packageData?.package?.id}
                datesArray={packageData?.package?.dates}
              />
            </Stack>
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default PackageDetails;
