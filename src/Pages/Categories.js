import { Box, FormControl, Stack, TextField, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import {
  useCreateCategoryMutation,
  useDeleteCategoryMutation,
  useEditCategoryMutation,
  useGetCategoryQuery,
} from "../Redux/API/CategoryAPI";
import Loader from "../UI/Loader";
import { ThemeColor } from "../Theme/Theme";
import Table from "../UI/Table";
import Button from "../UI/Button";
import PopupLayout from "../Layout/PopupLayout";
import { IconButton, Tooltip } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { useNavigate } from "react-router-dom";
import { openNotification } from "../Redux/Slices/NotificationSlice";
import { useDispatch } from "react-redux";
import debounce from "lodash.debounce";
import DeleteIcon from "@mui/icons-material/Delete";

const Categories = () => {
  const navigate = useNavigate();
  const columns_array = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "name",
      header: "Name",
      size: 150,
    },
    {
      accessorKey: "created_at",
      header: "Created At",
      size: 150,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },

    {
      accessorKey: "actions",
      header: "Actions",
      size: 150,

      Cell: ({ row }) => {
        return (
          <div>
            <Tooltip title="Edit">
              <IconButton
                onClick={() => {
                  openEditPopup();
                  setEditName({ name: row.original.name, id: row.original.id });
                }}
                aria-label="edit"
              >
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="show its courses and packages">
              <IconButton
                onClick={() => {
                  navigate(`/dashboard/category/${row.original.id}`);
                }}
                aria-label="show"
              >
                <VisibilityIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton
                disabled={deleteIsLoading}
                onClick={() => {
                  deleteCategory(row.original.id).then(() => {
                    refetch();
                  });
                }}
                aria-label="Delete"
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  const { data, isLoading, isError, error, refetch, isFetching } =
    useGetCategoryQuery();

  const [
    createCategory,
    {
      data: createData,
      isLoading: createLoading,
      isError: createIsError,
      isSuccess: createIsSuccess,
      error: createError,
    },
  ] = useCreateCategoryMutation();

  const [deleteCategory, { isLoading: deleteIsLoading }] =
    useDeleteCategoryMutation();

  const [
    editCategory,
    {
      data: editData,
      isLoading: editLoading,
      error: editError,
      isSuccess: editIsSuccess,
    },
  ] = useEditCategoryMutation();

  const [popupState, setPopupState] = useState(false);
  const [editPopupState, setEditPopupState] = useState(false);

  const [name, setName] = useState("");
  const [editName, setEditName] = useState({ id: 0, name: "" });

  const dispatch = useDispatch();

  useEffect(() => {
    if (createIsSuccess) {
      refetch();
      closePopup();
      dispatch(
        openNotification({
          message: "Category Added Successfully",
          status: "success",
        })
      );
    }
  }, [createIsSuccess]);

  useEffect(() => {
    if (editIsSuccess) {
      refetch();
      closeEditPopup();
    }
  }, [editIsSuccess]);

  const closePopup = () => {
    setPopupState(false);
  };
  const openPopup = () => {
    setPopupState(true);
  };

  const closeEditPopup = () => {
    setEditPopupState(false);
  };
  const openEditPopup = () => {
    setEditPopupState(true);
  };

  const debouncedSetName = debounce((newFormData) => {
    setName(newFormData);
  }, 300);

  const handleCreate = (e) => {
    e.preventDefault();
    createCategory({ name }).then((res) => {
      if (res?.error) {
        if (res.error.status) {
          dispatch(
            openNotification({
              message: "Network Error , Try Again",
              status: "error",
            })
          );
        } else
          dispatch(
            openNotification({ message: "Faild, Try Again", status: "error" })
          );
      }
    });
  };

  const handleEdit = (e) => {
    e.preventDefault();
    editCategory(editName).then((res) => {
      if (res?.error) {
        if (res.error.status) {
          dispatch(
            openNotification({
              message: "Network Error , Try Again",
              status: "error",
            })
          );
        } else
          dispatch(
            openNotification({ message: "Faild, Try Again", status: "error" })
          );
      }
    });
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <Box
      sx={{
        padding: "1.5rem",
      }}
    >
      {popupState && (
        <PopupLayout title={"Add Category"} closePopup={closePopup}>
          {createLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleCreate}>
              <TextField
                onChange={(e) => {
                  debouncedSetName(e.target.value);
                }}
                required
                size="small"
                label="category name"
              />
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Create</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {editPopupState && (
        <PopupLayout title={"Edit Category"} closePopup={closeEditPopup}>
          {editLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleEdit}>
              <TextField
                onChange={(e) => {
                  setEditName({ ...editName, name: e.target.value });
                }}
                value={editName.name}
                required
                size="small"
                label="category new name"
              />
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Edit</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      <Stack
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        mb={"1rem"}
      >
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.5rem",
            color: ThemeColor.main,
          }}
        >
          Categories
        </Typography>
        <Button onClick={openPopup}>Add category</Button>
      </Stack>
      <Table columns_array={columns_array} data={data?.categories} />
    </Box>
  );
};

export default Categories;
