import { Box, FormControl, Stack, TextField, Typography } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import {
  useCreateCategoryMutation,
  useEditCategoryMutation,
  useGetCategoryQuery,
} from "../../Redux/API/CategoryAPI";
import Loader from "../../UI/Loader";
import { ThemeColor } from "../../Theme/Theme";
import Table from "../../UI/Table";
import Button from "../../UI/Button";
import PopupLayout from "../../Layout/PopupLayout";
import { IconButton, Tooltip } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { useGetCourseQuery } from "../../Redux/API/CourseAPI";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import { useTheme } from "@mui/material/styles";
import UploadPhoto from "../../UI/UploadPhoto";
import debounce from "lodash.debounce";
import { Link } from "react-router-dom";
import {
  useCreatePackageMutation,
  useEditpackageMutation,
  useGetPackagesQuery,
  useSwitchActivityMutation,
} from "../../Redux/API/PackagesAPI";
import { openNotification } from "../../Redux/Slices/NotificationSlice";
import { useDispatch } from "react-redux";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Packages = () => {
  const [selectedCategories, setSelectedCategories] = React.useState([]);
  const [selectedCourses, setSelectedCourses] = React.useState([]);

  //Start Edit
  const [editPopupState, setEditPopupState] = useState(false);
  const [editPackageInfo, setEditPackageInfo] = useState({
    id: 0,
    title: "",
    discount: 0,
    price: 1,
    description: "",
    categories: [],
    src: null,
  });

  const closeEditPopup = () => {
    setEditPopupState(false);
  };
  const openEditPopup = () => {
    setEditPopupState(true);
  };
  const handleEdit = (e) => {
    e.preventDefault();
    editpackage({ ...editPackageInfo }).then((res) => {
      refetch();
      closeEditPopup();
      if (res?.error) {
        if (res.error.status) {
          dispatch(
            openNotification({
              message: "Network Error , Try Again",
              status: "error",
            })
          );
        } else
          dispatch(
            openNotification({ message: "Faild, Try Again", status: "error" })
          );
      } else {
        dispatch(
          openNotification({
            message: "Package Edited Successfully",
            status: "success",
          })
        );
      }
    });
  };

  const debouncedSetEditPackageInfo = debounce((newFormData) => {
    setEditPackageInfo(newFormData);
  }, 300);

  const handleEditFormChange = (e) => {
    const { name, value } = e.target;
    const newFormData = {
      ...editPackageInfo,
      [name]: value,
    };
    debouncedSetEditPackageInfo(newFormData);
  };

  const [editpackage, { isLoading: editLoading }] = useEditpackageMutation();

  // End Edit

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectedCategories(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const handleCoursesChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectedCourses(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  const columns_array = [
    {
      accessorKey: "id",
      header: "ID",
      size: 150,
    },
    {
      accessorKey: "title",
      header: "Name",
      size: 150,
    },

    {
      accessorKey: "price",
      header: "Price",
      size: 150,
      Cell: ({ cell }) => {
        return (
          <span>
            {Number(cell.getValue()).toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "address",
      header: "Address",
      size: 150,
    },
    {
      accessorKey: "discount",
      header: "Discount",
      size: 150,
      Cell: ({ cell }) => {
        return <span>{Number(cell.getValue()).toFixed(2)}%</span>;
      },
    },
    {
      accessorKey: "price_after_discount",
      header: "Price After Discount",
      size: 150,
      Cell: ({ cell }) => {
        console.log(cell.getValue());
        return (
          <span>
            {Number(cell.getValue()).toFixed(2)}{" "}
            <span style={{ fontSize: "0.7rem", fontWeight: "bold" }}>EURO</span>
          </span>
        );
      },
    },
    {
      accessorKey: "created_at",
      header: "Created At",
      size: 150,
      Cell: ({ cell }) => {
        let dateObj = new Date(cell.getValue());
        let formattedDate =
          dateObj.toLocaleDateString() + " " + dateObj.toLocaleTimeString();
        return <span>{formattedDate}</span>;
      },
    },

    {
      accessorKey: "actions",
      header: "Actions",
      size: 250,

      Cell: ({ row }) => {
        return (
          <div>
            <Tooltip title="Edit">
              <IconButton
                onClick={() => {
                  setEditPackageInfo({
                    id: row.original.id,
                    price: row.original.price,
                    discount: row.original.discount,
                    src: row.original.src,
                    address: row.original.address,
                    title: row.original.title,
                    description: row.original.description,
                  });

                  openEditPopup();
                }}
                aria-label="edit"
              >
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="show package details">
              <Link
                style={{
                  color: "#757575",
                  margin: "0",
                  padding: "0",
                }}
                to={`/dashboard/package/${row.original.id}`}
              >
                <IconButton aria-label="show">
                  <VisibilityIcon />
                </IconButton>
              </Link>
            </Tooltip>

            <Tooltip title="click to switch">
              <IconButton aria-label="show">
                <button
                  disabled={loadingActive}
                  style={{
                    background: loadingActive
                      ? "#e5e5e5"
                      : row.original.active == 1
                      ? "#037403"
                      : "#ca0606",
                    padding: "0.3rem 0.5rem",
                    border: "none",
                    outline: "none",
                    color: loadingActive ? "#bdbdbd" : "#eee",
                    borderRadius: "3px",
                    transition: "0.3s",
                    cursor: "pointer",
                    marginLeft: "0.3rem",
                    fontSize: "0.7rem",
                  }}
                  onClick={(e) => {
                    switchActivity(row.original.id)
                      .then((resp) => {
                        console.log("hello");
                        refetch();
                      })
                      .catch(() => {
                        console.log("error");
                      });
                  }}
                >
                  {row.original.active == 1 ? (
                    <span>Active</span>
                  ) : (
                    <span>Not Active</span>
                  )}
                </button>
              </IconButton>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  const [
    switchActivity,
    {
      isLoading: loadingActive,
      isSuccess: isSuccessActive,
      isError: isErrorActive,
    },
  ] = useSwitchActivityMutation();
  const { data, isLoading, isError, error, refetch, isFetching } =
    useGetPackagesQuery();
  const { data: categories, isLoading: loadingCategories } =
    useGetCategoryQuery();
  const { data: courses, isLoading: loadingCourses } = useGetCourseQuery();

  const [
    createPackage,
    {
      data: createData,
      isLoading: createLoading,
      error: createError,
      isSuccess: createIsSuccess,
    },
  ] = useCreatePackageMutation();

  const [popupState, setPopupState] = useState(false);

  const [packageInfo, setPackageInfo] = useState({
    title: "",
    discount: 0,
    address: "",
    description: "",
    price: 1,
    src: null,
    categories: [],
  });

  useEffect(() => {
    if (createIsSuccess) {
      refetch();
      closePopup();
    }
  }, [createIsSuccess]);

  const closePopup = () => {
    setPopupState(false);
  };
  const openPopup = () => {
    setPopupState(true);
  };

  const dispatch = useDispatch();

  const [errorMessage, setErrorMessage] = useState({
    src: false,
    cat: false,
    courses: false,
  });

  const handleCreate = (e) => {
    e.preventDefault();

    if (selectedCourses.length == 0) {
      setErrorMessage({ ...errorMessage, courses: true });
      return;
    }
    if (selectedCategories.length == 0) {
      setErrorMessage({ ...errorMessage, cat: true });
      return;
    }
    if (packageInfo.src == null) {
      setErrorMessage({ ...errorMessage, src: true });
      return;
    }

    setErrorMessage({ src: false, cat: false });

    createPackage({
      ...packageInfo,
      categories: selectedCategories,
      courses: selectedCourses,
    }).then((res) => {
      if (res?.error) {
        if (res.error.status) {
          dispatch(
            openNotification({
              message: "Network Error , Try Again",
              status: "error",
            })
          );
        } else
          dispatch(
            openNotification({ message: "Faild, Try Again", status: "error" })
          );
      } else {
        dispatch(
          openNotification({
            message: "Package Added Successfully",
            status: "success",
          })
        );
      }
    });
  };

  const debouncedSetFormData = debounce((newFormData) => {
    setPackageInfo(newFormData);
  }, 300);

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    const newFormData = {
      ...packageInfo,
      [name]: value,
    };
    debouncedSetFormData(newFormData);
  };

  const handleUpladPic = (image) => {
    setPackageInfo((prev) => {
      return { ...prev, src: image };
    });
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <Box
      sx={{
        padding: "1.5rem",
      }}
    >
      {popupState && (
        <PopupLayout title={"Add Package"} closePopup={closePopup}>
          {createLoading || (loadingCategories && loadingCourses) ? (
            <Loader />
          ) : categories?.categories?.length === 0 ? (
            <Typography>Please Add Category First</Typography>
          ) : courses?.courses?.length === 0 ? (
            <Typography>Please Add Courses First</Typography>
          ) : (
            <form onSubmit={handleCreate}>
              <Stack gap={"0.8rem"}>
                <TextField
                  name="title"
                  onChange={handleFormChange}
                  required
                  size="small"
                  label="Title"
                />
                <TextField
                  name="address"
                  onChange={handleFormChange}
                  required
                  size="small"
                  label="Address"
                />
                <TextField
                  name="price"
                  onChange={handleFormChange}
                  required
                  size="small"
                  type="number"
                  label="price"
                  inputProps={{ min: 0.1, step: "any" }}
                />
                <TextField
                  name="discount"
                  onChange={handleFormChange}
                  required
                  size="small"
                  type="number"
                  label="discount"
                  inputProps={{ min: 0, max: 100 }}
                />
                <TextField
                  name="description"
                  onChange={handleFormChange}
                  required
                  multiline
                  rows={"3"}
                  size="small"
                  label="description"
                />

                <FormControl>
                  <InputLabel id="demo-multiple-chip-label">Courses</InputLabel>
                  <Select
                    labelId="demo-multiple-chip-label"
                    id="demo-multiple-chip"
                    multiple
                    value={selectedCourses}
                    onChange={handleCoursesChange}
                    style={{}}
                    input={
                      <OutlinedInput
                        id="select-multiple-chip"
                        label="Courses"
                      />
                    }
                    renderValue={(selected) => (
                      <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                        {selected.map((value) => (
                          <Chip
                            key={value}
                            label={
                              courses.courses.find((item) => item.id == value)
                                .title
                            }
                          />
                        ))}
                      </Box>
                    )}
                    MenuProps={MenuProps}
                  >
                    {courses?.courses?.map((course) => (
                      <MenuItem
                        key={course.id}
                        value={course.id}
                        //   style={getStyles(name, personName, theme)}
                        style={{ zIndex: "10000" }}
                      >
                        {course.title}
                      </MenuItem>
                    ))}
                  </Select>
                  {errorMessage.courses && (
                    <Typography color="error">please add course</Typography>
                  )}
                </FormControl>
                <FormControl>
                  <InputLabel id="demo-multiple-chip-label">
                    Categories
                  </InputLabel>
                  <Select
                    labelId="demo-multiple-chip-label"
                    id="demo-multiple-chip"
                    multiple
                    value={selectedCategories}
                    onChange={handleChange}
                    style={{}}
                    input={
                      <OutlinedInput
                        id="select-multiple-chip"
                        label="Categories"
                      />
                    }
                    renderValue={(selected) => (
                      <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                        {selected.map((value) => (
                          <Chip
                            key={value}
                            label={
                              categories.categories.find(
                                (item) => item.id == value
                              ).name
                            }
                          />
                        ))}
                      </Box>
                    )}
                    MenuProps={MenuProps}
                  >
                    {categories?.categories?.map((cat) => (
                      <MenuItem
                        key={cat.id}
                        value={cat.id}
                        //   style={getStyles(name, personName, theme)}
                        style={{ zIndex: "10000" }}
                      >
                        {cat.name}
                      </MenuItem>
                    ))}
                  </Select>
                  {errorMessage.cat && (
                    <Typography color="error">please add category</Typography>
                  )}
                  <UploadPhoto handleUpladPic={handleUpladPic} />
                  {errorMessage.src && (
                    <Typography color="error">please add image</Typography>
                  )}
                </FormControl>
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Create</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      {editPopupState && (
        <PopupLayout title={"Edit Course"} closePopup={closeEditPopup}>
          {editLoading ? (
            <Loader />
          ) : (
            <form onSubmit={handleEdit}>
              <Stack gap={"0.8rem"}>
                <TextField
                  name="title"
                  onChange={handleEditFormChange}
                  required
                  defaultValue={editPackageInfo.title}
                  size="small"
                  label="Title"
                />
                <TextField
                  name="address"
                  onChange={handleEditFormChange}
                  defaultValue={editPackageInfo.address}
                  required
                  size="small"
                  label="Address"
                />
                <TextField
                  name="price"
                  onChange={handleEditFormChange}
                  defaultValue={editPackageInfo.price}
                  required
                  size="small"
                  type="number"
                  label="price"
                  inputProps={{ min: 1 }}
                />
                <TextField
                  name="discount"
                  onChange={handleEditFormChange}
                  defaultValue={editPackageInfo.discount}
                  required
                  size="small"
                  type="number"
                  label="discount"
                  inputProps={{ min: 0, max: 100 }}
                />
                <TextField
                  name="description"
                  defaultValue={editPackageInfo.description}
                  onChange={handleEditFormChange}
                  required
                  multiline
                  rows={"5"}
                  size="small"
                  label="description"
                />
              </Stack>
              <div style={{ margin: "1rem 0" }}>
                <Button type="submit">Edit</Button>
              </div>
            </form>
          )}
        </PopupLayout>
      )}

      <Stack
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        mb={"1rem"}
      >
        <Typography
          sx={{
            fontWeight: "bold",
            fontSize: "1.5rem",
            color: ThemeColor.main,
          }}
        >
          Packages
        </Typography>
        <Button onClick={openPopup}>Add package</Button>
      </Stack>
      <Table columns_array={columns_array} data={data?.packages} />
    </Box>
  );
};

export default Packages;
